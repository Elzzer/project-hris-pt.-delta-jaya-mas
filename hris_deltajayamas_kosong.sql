-- phpMyAdmin SQL Dump
-- version 4.9.0.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Sep 07, 2020 at 05:15 AM
-- Server version: 10.4.6-MariaDB
-- PHP Version: 7.3.9

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `hris_deltajayamas`
--
CREATE DATABASE IF NOT EXISTS `hris_deltajayamas` DEFAULT CHARACTER SET latin1 COLLATE latin1_swedish_ci;
USE `hris_deltajayamas`;

-- --------------------------------------------------------

--
-- Table structure for table `absensi`
--

DROP TABLE IF EXISTS `absensi`;
CREATE TABLE `absensi` (
  `kode_absen` int(11) NOT NULL,
  `kode_user` int(11) NOT NULL,
  `absent_date` date NOT NULL,
  `absent_in` varchar(10) NOT NULL,
  `absent_out` varchar(10) NOT NULL,
  `attachment` int(15) NOT NULL,
  `status_terlambat` int(1) NOT NULL,
  `status_pulang_cepat` int(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `evaluation_skill`
--

DROP TABLE IF EXISTS `evaluation_skill`;
CREATE TABLE `evaluation_skill` (
  `id_evaluation_skill` int(11) NOT NULL,
  `experience` text NOT NULL,
  `project` text NOT NULL,
  `teamwork` text NOT NULL,
  `hard_skill` text NOT NULL,
  `soft_skill` text NOT NULL,
  `note_evaluation_skill` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `foto`
--

DROP TABLE IF EXISTS `foto`;
CREATE TABLE `foto` (
  `id_foto` int(11) NOT NULL,
  `id_pemilik` int(15) NOT NULL,
  `jenis` varchar(255) NOT NULL,
  `jenis_foto` varchar(255) NOT NULL,
  `split1` longtext NOT NULL,
  `split2` longtext NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `gaji`
--

DROP TABLE IF EXISTS `gaji`;
CREATE TABLE `gaji` (
  `kode_user` int(11) NOT NULL,
  `basic_salary` int(15) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `jabatan`
--

DROP TABLE IF EXISTS `jabatan`;
CREATE TABLE `jabatan` (
  `kode_user` int(11) NOT NULL,
  `department` varchar(255) NOT NULL,
  `position` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `m_recruitment`
--

DROP TABLE IF EXISTS `m_recruitment`;
CREATE TABLE `m_recruitment` (
  `kode_recruitment` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `address` varchar(255) NOT NULL,
  `tanggal_lahir` date NOT NULL,
  `city` varchar(255) NOT NULL,
  `phone` varchar(255) NOT NULL,
  `education` varchar(255) NOT NULL,
  `department` varchar(255) NOT NULL,
  `position` varchar(255) NOT NULL,
  `status` varchar(255) NOT NULL,
  `gender` varchar(255) NOT NULL,
  `id_foto` int(11) NOT NULL,
  `id_evaluation_skill` int(11) NOT NULL,
  `id_negotiation` int(11) NOT NULL,
  `diterima` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `negotiations`
--

DROP TABLE IF EXISTS `negotiations`;
CREATE TABLE `negotiations` (
  `id_negotiation` int(11) NOT NULL,
  `contract` text NOT NULL,
  `requirement` text NOT NULL,
  `salary` int(15) NOT NULL,
  `note_negotiation` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `penggajian`
--

DROP TABLE IF EXISTS `penggajian`;
CREATE TABLE `penggajian` (
  `kode_penggajian` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `nokaryawan` varchar(255) NOT NULL,
  `status` varchar(255) NOT NULL,
  `gaji` int(15) NOT NULL,
  `department` varchar(255) NOT NULL,
  `bulan` varchar(255) NOT NULL,
  `tahun` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

DROP TABLE IF EXISTS `user`;
CREATE TABLE `user` (
  `kode_user` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `address` varchar(255) NOT NULL,
  `birth_date` date NOT NULL,
  `city` varchar(255) NOT NULL,
  `phone` varchar(20) NOT NULL,
  `password` varchar(255) NOT NULL,
  `education` varchar(255) NOT NULL,
  `status` varchar(255) NOT NULL,
  `gender` varchar(255) NOT NULL,
  `status_kerja` int(1) NOT NULL,
  `jurusan_sekolah` varchar(255) NOT NULL,
  `nama_sekolah` varchar(255) NOT NULL,
  `nik` varchar(255) NOT NULL,
  `nokaryawan` varchar(255) NOT NULL,
  `npwp` varchar(255) NOT NULL,
  `nobca` varchar(255) NOT NULL,
  `nokesehatan` varchar(255) NOT NULL,
  `noketenagakerjaan` varchar(255) NOT NULL,
  `tglkerja` date NOT NULL,
  `status_karyawan` varchar(6) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `absensi`
--
ALTER TABLE `absensi`
  ADD PRIMARY KEY (`kode_absen`);

--
-- Indexes for table `evaluation_skill`
--
ALTER TABLE `evaluation_skill`
  ADD PRIMARY KEY (`id_evaluation_skill`);

--
-- Indexes for table `foto`
--
ALTER TABLE `foto`
  ADD PRIMARY KEY (`id_foto`);

--
-- Indexes for table `gaji`
--
ALTER TABLE `gaji`
  ADD PRIMARY KEY (`kode_user`);

--
-- Indexes for table `jabatan`
--
ALTER TABLE `jabatan`
  ADD PRIMARY KEY (`kode_user`);

--
-- Indexes for table `m_recruitment`
--
ALTER TABLE `m_recruitment`
  ADD PRIMARY KEY (`kode_recruitment`);

--
-- Indexes for table `negotiations`
--
ALTER TABLE `negotiations`
  ADD PRIMARY KEY (`id_negotiation`);

--
-- Indexes for table `penggajian`
--
ALTER TABLE `penggajian`
  ADD PRIMARY KEY (`kode_penggajian`);

--
-- Indexes for table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`kode_user`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `absensi`
--
ALTER TABLE `absensi`
  MODIFY `kode_absen` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `evaluation_skill`
--
ALTER TABLE `evaluation_skill`
  MODIFY `id_evaluation_skill` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `foto`
--
ALTER TABLE `foto`
  MODIFY `id_foto` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `m_recruitment`
--
ALTER TABLE `m_recruitment`
  MODIFY `kode_recruitment` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `negotiations`
--
ALTER TABLE `negotiations`
  MODIFY `id_negotiation` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `penggajian`
--
ALTER TABLE `penggajian`
  MODIFY `kode_penggajian` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `user`
--
ALTER TABLE `user`
  MODIFY `kode_user` int(11) NOT NULL AUTO_INCREMENT;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
