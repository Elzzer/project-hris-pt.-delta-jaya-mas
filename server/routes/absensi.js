const express = require("express");
const config = require('./config');
const mysql= require('mysql');
const app= express();
const pool= mysql.createPool(config.database);
const jwt= require('jwt-simple');
const upload = require('./upload');
const fs= require('fs');

const router = express.Router();

const { promisify } = require('util');
// const getConnection= () => {
//     return new Promise((resolve, reject) => {
//         pool.getConnection((err, conn) => {
//             if (err) reject(err);
//             else resolve(conn);
//         });
//     });
// };

// const executeQuery= (conn, query) => {
//     return new Promise((resolve, reject) => {
//         conn.query(query, (err, res) => {
//             if (err) reject(err);
//             else resolve(res);
//         });
//     });
// };

router.post("/absenIn", async (req,res) => {
    let token = req.body.token;
    let dataUser = jwt.decode(token, 'user');

    let promisfydbconnection = promisify(pool.query).bind(pool);
    let result = await promisfydbconnection( `select * from user where phone = '${dataUser.phone}'`);

    let today = new Date();
    let date = today.getFullYear()+'-'+(today.getMonth()+1)+'-'+today.getDate();
    let result2 = await promisfydbconnection( `select * from absensi where absent_date='${date}' and kode_user=${result[0].kode_user}`);

    if(result2.length == 0){
        var hournow = new Date();
        var timenow = hournow.getHours().toString().padStart(2, '0') + ":" + hournow.getMinutes().toString().padStart(2, '0') + ":" + hournow.getSeconds().toString().padStart(2, '0');
        // console.log("insert : " + date+ " | " + timenow);

        //TEPAT WAKTU
        if(hournow.getHours() == 7 && hournow.getMinutes() >= 0 && hournow.getMinutes() <= 30){
            let insert1 = await promisfydbconnection( 
                `INSERT INTO absensi
                VALUES (null, ${result[0].kode_user}, '${date}', '${timenow}', '', '', 0, -1)`);
            
            res.json([{pesan:"Absen Masuk telah tercatat (Tepat Waktu)"}]);
        }
        //TERLAMBAT
        else if(hournow.getHours() >= 7){
            let insert1 = await promisfydbconnection(
                `INSERT INTO absensi
                VALUES (null, ${result[0].kode_user}, '${date}', '${timenow}', '', '', 1, -1)`);
            
            res.json([{pesan:"Absen Masuk telah tercatat (Terlambat)"}]);
        }
    }
    // else {
    //     console.log("java : " + date);
    //     console.log("db : " + result2[0].absent_date + " | " + result2[0].absent_in);
    // }
})

router.get("/getAbsensiUser", async (req,res) => {
    let token = req.query.token;
    let dataUser = jwt.decode(token, 'user');

    //TANGGAL
    let today = new Date();
    let date = today.getFullYear().toString().padStart(2, '0') + '-'+(today.getMonth()+1).toString().padStart(2, '0') + '-'+today.getDate().toString().padStart(2, '0');
    // console.log(result[0].kode_user + " | " + date);

    let promisfydbconnection = promisify(pool.query).bind(pool);
    let result = await promisfydbconnection( 
        `SELECT u.name "name" , f.split1 "split1", f.split2 "split2"
        , DATE_FORMAT(CAST(a.absent_date AS CHAR), '%Y-%m-%d') "absent_date", a.absent_in "absent_in", a.status_pulang_cepat "status_pulang_cepat"
        FROM user u, absensi a , foto f
        where phone = '${dataUser.phone}' and u.kode_user = a.kode_user and u.kode_user = f.id_pemilik and a.absent_date='${date}'`);
    res.json([{ hasil:result[0] }]);
})

router.post("/absenOut", async (req,res) => {
    let token = req.body.token;
    let dataUser = jwt.decode(token, 'user');

    let promisfydbconnection = promisify(pool.query).bind(pool);
    let result = await promisfydbconnection(`select * from user where phone = '${dataUser.phone}'`);

    //JAM
    var hournow = new Date();
    var timenow = hournow.getHours().toString().padStart(2, '0') + ":" + hournow.getMinutes().toString().padStart(2, '0') + ":" + hournow.getSeconds().toString().padStart(2, '0');
    //TANGGAL
    let today = new Date();
    let date = today.getFullYear().toString().padStart(2, '0') + '-'+(today.getMonth()+1).toString().padStart(2, '0') + '-'+today.getDate().toString().padStart(2, '0');
    // console.log(result[0].kode_user + " | " + date);

    let absenout = await promisfydbconnection(
        `UPDATE absensi 
        SET absent_out='${timenow}', status_pulang_cepat=0
        WHERE kode_user=${result[0].kode_user} and absent_date='${date}'`);
    
    res.json([{pesan:"Absen Keluar telah tercatat"}]);
})

router.post("/absenOutWithAttachment", upload.single("image"), async (req, res) => {
    
    let token = req.body.token;
    let dataUser = jwt.decode(token, 'user');

    let promisfydbconnection = promisify(pool.query).bind(pool);
    let profil_picture=req.file;
    fs.readFile(profil_picture.path, async function(err, data) {
        if (err) throw err;
        var encodedImage = Buffer.from(data).toString('base64');
            if(encodedImage.length<1500000){
                let split=profil_picture.filename.split(".");
                if(split[1].toUpperCase()=="JPEG" || split[1].toUpperCase()=="JPG" || split[1].toUpperCase()=="PNG"){
                    let resultuser = await promisfydbconnection( `select * from user where phone = '${dataUser.phone}'`);

                    //JAM
                    var hournow = new Date();
                    var timenow = hournow.getHours().toString().padStart(2, '0') + ":" + hournow.getMinutes().toString().padStart(2, '0') + ":" + hournow.getSeconds().toString().padStart(2, '0');
                    //TANGGAL
                    let today = new Date();
                    let date = today.getFullYear().toString().padStart(2, '0') + '-'+(today.getMonth()+1).toString().padStart(2, '0') + '-'+today.getDate().toString().padStart(2, '0');
                    // console.log(resultuser[0].kode_user + " | " + date);
    
                    result1 = await promisfydbconnection( 
                        `INSERT INTO foto
                        VALUES(null , ${resultuser[0].kode_user}, '${"ijin_absen"}', '${split[1]}', '${encodedImage.substring(0,800000)}','${encodedImage.substring(800000)}')`
                    );
                    
                    let max_id = await promisfydbconnection( `select max(id_foto) "id_foto" from foto`);
    
                    let absenout = await promisfydbconnection(
                        `UPDATE absensi 
                        SET absent_out='${timenow}', status_pulang_cepat=1, attachment=${max_id[0].id_foto}
                        WHERE kode_user=${resultuser[0].kode_user} and absent_date='${date}'`);
    
                    fs.unlinkSync(profil_picture.path);
                    return res.json([{pesan:"Absen Keluar telah tercatat"}]);
                }
                else{
                    fs.unlinkSync(profil_picture.path);
                    let kembalian = [
                        {pesan:"File yang diupload bukan foto"}
                    ];
                    return res.json(kembalian);
                }
            }
            else{
                fs.unlinkSync(profil_picture.path);
                let kembalian = [
                    {pesan:"Ukuran Foto Lebih Dari 1 MB"}
                ];
                return res.json(kembalian);
            }
    });
});

router.post("/logout", async (req,res) => {
    let token = req.body.token;
    let dataUser = jwt.decode(token, 'user');

    let promisfydbconnection = promisify(pool.query).bind(pool);
    let result = await promisfydbconnection( `select * from user where phone = '${dataUser.phone}'`);

    //JAM
    var hournow = new Date();
    var timenow = hournow.getHours().toString().padStart(2, '0') + ":" + hournow.getMinutes().toString().padStart(2, '0') + ":" + hournow.getSeconds().toString().padStart(2, '0');
    //TANGGAL
    let today = new Date();
    let date = today.getFullYear().toString().padStart(2, '0') + '-'+(today.getMonth()+1).toString().padStart(2, '0') + '-'+today.getDate().toString().padStart(2, '0');
    //console.log(result[0].kode_user + " | " + date);

    let absenout = await promisfydbconnection(
        `UPDATE absensi 
        SET absent_out='${timenow}'
        WHERE kode_user=${result[0].kode_user} and absent_date='${date}' and status_pulang_cepat=-1`);
    
    res.json([{pesan:"Logout!!!"}]);
})

module.exports = router;