const express = require("express");
const config = require('./config');
const mysql= require('mysql');
const app= express();
const pool= mysql.createPool(config.database);
const jwt= require('jwt-simple');
const upload = require('./upload');
const fs= require('fs');

const router = express.Router();

const { promisify } = require('util');

// const getConnection= () => {
//     return new Promise((resolve, reject) => {
//         pool.getConnection((err, conn) => {
//             if (err) reject(err);
//             else resolve(conn);
//         });
//     });
// };

// const executeQuery= (conn, query) => {
//     return new Promise((resolve, reject) => {
//         conn.query(query, (err, res) => {
//             if (err) reject(err);
//             else resolve(res);
//         });
//     });
// };

router.get("/getDataLaporan", async (req,res) => {
    let promisfydbconnection = promisify(pool.query).bind(pool);

    //OLD
    // //GET ALL SALARY
    // let result = await executeQuery(conn, 
    //     `SELECT sum(gaji) "total", MONTH(tanggal) "month", YEAR(tanggal) "year"
    //     from penggajian
    //     group by MONTH(tanggal), YEAR(tanggal)
    //     order by tanggal desc`);

    
    // //GET ALL KEUANGAN
    // let result2 = await executeQuery(conn, 
    //     `SELECT mk.nominal "total", DATE_FORMAT(CAST(mk.tanggal AS CHAR), '%d %M %Y') "tanggal", u.name "name", mk.masalah "masalah"
    //             , f.split1 "split1", f.split2 "split2"
    //     from m_keuangan mk, user u, foto f
    //     where mk.status = 1 and mk.kode_user = u.kode_user and mk.id_foto = f.id_foto
    //     order by tanggal desc`);
    
    // //GET TOTAL SALARY AND KEUANGAN
    // let result3 = await executeQuery(conn, `select sum(gaji) "total" from penggajian`);
    // let result4 = await executeQuery(conn, `select sum(nominal) "total" from m_keuangan`);

    // res.json([
    //     {
    //         salary: result,
    //         keuangan: result2,
    //         total_salary: result3[0].total,
    //         total_keuangan: result4[0].total
    //     }
    // ]);

    //NEW
    //GET ALL SALARY
    // let result = await promisfydbconnection(
    //     `SELECT sum(p.gaji) "total", MONTH(p.tanggal) "month", YEAR(p.tanggal) "year", u.status_karyawan "status_karyawan"
    //     from penggajian p, user u
    //     where u.kode_user = p.kode_user
    //     group by MONTH(p.tanggal), YEAR(p.tanggal), u.status_karyawan
    //     order by p.tanggal desc`);

    
    // //GET ALL KEUANGAN
    // let result2 = await promisfydbconnection(
    //     `SELECT p.gaji "total", MONTH(p.tanggal) "month", YEAR(p.tanggal) "year", u.name "name", p.id_keuangan "id_keuangan"
    //             , f.split1 "split1", f.split2 "split2"
    //             , g.basic_salary "basic_salary", g.transport_allowance "transport_allowance", g.medical_allowance "medical_allowance", g.position_allowance "position_allowance"
    //     from penggajian p, user u, foto f, gaji g
    //     where   u.kode_user = p.kode_user and g.kode_user = u.kode_user
    //             and f.id_pemilik = u.kode_user and f.jenis = "foto_profil"
    //     order by p.tanggal desc, u.name`);
    
    // //GET TOTAL SALARY AND KEUANGAN
    // let result3 = await promisfydbconnection(`select sum(gaji) "total" from penggajian`);
    // let result5 = await promisfydbconnection(`select * from m_keuangan`);
    
    // res.json([
    //     {
    //         salary: result,
    //         keuangan: result2,
    //         total_salary: result3[0].total,
    //         semua_keuangan: result5
    //     }
    // ]);


    //REVISI
    //GET ALL SALARY
    let result = await promisfydbconnection(
        `SELECT sum(gaji) "total", bulan "month", tahun "year", status "status_karyawan"
        from penggajian
        group by bulan, tahun, status
        order by tahun desc, bulan desc`);

    //GET ALL KEUANGAN
    let result2 = await promisfydbconnection(
        `SELECT gaji "total", bulan "month", tahun "year", name "name", kode_penggajian "id_keuangan", nokaryawan "nokaryawan", status "status", department "department"
        from penggajian
        order by tahun desc, bulan desc`);

    //GET TOTAL SALARY AND KEUANGAN
    let result3 = await promisfydbconnection(`select sum(gaji) "total" from penggajian`);
    res.json([
        {
            salary: result,
            keuangan: result2,
            total_salary: result3[0].total
        }
    ]);
})

router.get("/getDataChart", async (req,res) => {
    // let promisfydbconnection = promisify(pool.query).bind(pool);
    // let result = await promisfydbconnection( 
    //     `SELECT u.name "nama", sum(mk.nominal) "jumlah"
    //     FROM user u, m_keuangan mk 
    //     WHERE mk.kode_user = u.kode_user  and mk.status=1
    //     GROUP BY u.name`)

    // let jumlah = [];
    // let nama = [];
    // result.forEach(function(element) {
    //     jumlah.push(parseInt(element.jumlah));
    //     nama.push(element.nama);
    // });
    // let kembalian = [{jumlah:jumlah,nama:nama}];
    // res.json(kembalian);

    let month = req.query.month;
    let year = req.query.year;
    let promisfydbconnection = promisify(pool.query).bind(pool);
    let nama = ['PKWT','PKWTT','OS'];
    let jumlah = [];

    //SUM GAJI PKWT
    let result = await promisfydbconnection(`SELECT IFNULL(sum(gaji), 0) "gaji" from penggajian where status='PKWT' and bulan='${month}' and tahun='${year}'`);
    jumlah.push(result[0].gaji)
    
    //SUM GAJI PKWTT
    result = await promisfydbconnection(`SELECT IFNULL(sum(gaji), 0) "gaji" from penggajian where status='PKWTT' and bulan='${month}' and tahun='${year}'`);
    jumlah.push(result[0].gaji)
    
    //SUM GAJI OS
    result = await promisfydbconnection(`SELECT IFNULL(sum(gaji), 0) "gaji" from penggajian where status='OS' and bulan='${month}' and tahun='${year}'`);
    jumlah.push(result[0].gaji)

    let kembalian = [{jumlah:jumlah,nama:nama}];
    res.json(kembalian);
})

router.get("/getLaporanFilter", async (req, res) => {
    // let tglFrom = "";
    // let tglUntil = "";
    // let temp = req.query.tglFrom.split("-");
    // let temp2 = req.query.tglUntil.split("-");
    
    // if(req.query.tglFrom != "") tglFrom = temp[0] + "/" + temp[1] + "/" + temp[2];
    // else tglFrom = "1900/01/01";
    // if(req.query.tglUntil != "") tglUntil = temp2[0] + "/" + temp2[1] + "/" + temp2[2];
    // else tglUntil = "2100/01/01";
    // //console.log(tglFrom + " | " + tglUntil)

    // let conn= await getConnection();
    // //GET ALL SALARY
    // let result = await executeQuery(conn, 
    //     `SELECT sum(gaji) "total", MONTH(tanggal) "month", YEAR(tanggal) "year"
    //     from penggajian
    //     where tanggal >= '${tglFrom}' and tanggal <= '${tglUntil}'
    //     group by MONTH(tanggal), YEAR(tanggal)
    //     order by tanggal desc`);

    
    // //GET ALL KEUANGAN
    // let result2 = await executeQuery(conn, 
    //     `SELECT mk.nominal "total", DATE_FORMAT(CAST(mk.tanggal AS CHAR), '%d %M %Y') "tanggal", u.name "name", mk.masalah "masalah"
    //             , f.split1 "split1", f.split2 "split2"
    //     from m_keuangan mk, user u, foto f
    //     where mk.status = 1 and mk.kode_user = u.kode_user and mk.id_foto = f.id_foto and mk.tanggal >= '${tglFrom}' and mk.tanggal <= '${tglUntil}'
    //     order by tanggal desc`);
    
    // //GET TOTAL SALARY AND KEUANGAN
    // let result3 = await executeQuery(conn, `select sum(gaji) "total" from penggajian where tanggal >= '${tglFrom}' and tanggal <= '${tglUntil}'`);
    // let result4 = await executeQuery(conn, `select sum(nominal) "total" from m_keuangan where tanggal >= '${tglFrom}' and tanggal <= '${tglUntil}'`);
    // res.json([
    //     {
    //         salary: result,
    //         keuangan: result2,
    //         total_salary: result3[0].total,
    //         total_keuangan: result4[0].total
    //     }
    // ]);


    // //NEW
    // let month = req.query.month;
    // let year = req.query.year;
    // // if(month == "") month = " ";
    // // if(year == "") year = " ";
    // //console.log(month + " | " + year);

    // let promisfydbconnection = promisify(pool.query).bind(pool);
    // //GET ALL SALARY
    // let result = await promisfydbconnection( 
    //     `SELECT sum(p.gaji) "total", MONTH(p.tanggal) "month", YEAR(p.tanggal) "year", u.status_karyawan "status_karyawan"
    //     from penggajian p, user u
    //     where   u.kode_user = p.kode_user
    //             and MONTH(p.tanggal) like '%${month}%' and  YEAR(p.tanggal) like '%${year}%'
    //     group by MONTH(p.tanggal), YEAR(p.tanggal), u.status_karyawan
    //     order by p.tanggal desc`);

    // //GET ALL KEUANGAN
    // let result2 = await promisfydbconnection(
    //     `SELECT p.gaji "total", MONTH(p.tanggal) "month", YEAR(p.tanggal) "year", u.name "name", p.id_keuangan "id_keuangan"
    //             , f.split1 "split1", f.split2 "split2"
    //             , g.basic_salary "basic_salary", g.transport_allowance "transport_allowance", g.medical_allowance "medical_allowance", g.position_allowance "position_allowance"
    //     from penggajian p, user u, foto f, gaji g
    //     where   u.kode_user = p.kode_user and g.kode_user = u.kode_user
    //             and f.id_pemilik = u.kode_user and f.jenis = "foto_profil"
    //             and MONTH(p.tanggal) like '%${month}%' and YEAR(p.tanggal) like '%${year}%'
    //     order by p.tanggal desc, u.name`);
    
    // //GET TOTAL SALARY AND KEUANGAN
    // let result3 = await promisfydbconnection( `select sum(gaji) "total" from penggajian where MONTH(tanggal)like '%${month}%' and YEAR(tanggal) like '%${year}%'`);
    // res.json([
    //     {
    //         salary: result,
    //         keuangan: result2,
    //         total_salary: result3[0].total
    //     }
    // ]);

    //REVISI
    let month = req.query.month;
    let year = req.query.year;
    let promisfydbconnection = promisify(pool.query).bind(pool);

    //GET ALL SALARY
    let result = await promisfydbconnection( 
        `SELECT sum(gaji) "total", bulan "month", tahun "year", status "status_karyawan"
        from penggajian
        where bulan like '%${month}%' and tahun like '%${year}%'
        group by bulan, tahun, status
        order by tahun desc, bulan desc`);
    
    //GET ALL KEUANGAN
    let result2 = await promisfydbconnection(
        `SELECT gaji "total", bulan "month", tahun "year", name "name", kode_penggajian "id_keuangan", nokaryawan "nokaryawan", status "status", department "department"
        from penggajian
        where bulan like '%${month}%' and tahun like '%${year}%'
        order by tahun desc, bulan desc`);

    //GET TOTAL SALARY AND KEUANGAN
    let result3 = await promisfydbconnection(`select sum(gaji) "total" from penggajian where bulan like '%${month}%' and tahun like '%${year}%'`);
    
    res.json([
        {
            salary: result,
            keuangan: result2,
            total_salary: result3[0].total
        }
    ]);
});


//GET DETAIL SALARY
router.get("/getDetailSalary", async (req,res) => {
    let month = req.query.month;
    let year = req.query.year;
    let status = req.query.status;

    let promisfydbconnection = promisify(pool.query).bind(pool);
    let result = await promisfydbconnection(
        `SELECT *
        from penggajian 
        where bulan = ${month} and tahun = ${year} and status='${status}'`);
    
    res.json([
        {
            hasil: result
        }
    ]);
})

module.exports = router;

//SELECT IFNULL(sum(gaji), 0) from penggajian where status='OS' and bulan='5'