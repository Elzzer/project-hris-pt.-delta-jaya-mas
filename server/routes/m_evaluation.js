const express = require("express");
const config = require('./config');
const mysql= require('mysql');
const app = express();
const pool= mysql.createPool(config.database);
const jwt= require('jwt-simple');

const router = express.Router();
const { promisify } = require('util');

// const getConnection= () => {
//     return new Promise((resolve, reject) => {
//         pool.getConnection((err, conn) => {
//             if (err) reject(err);
//             else resolve(conn);
//         });
//     });
// };

// const executeQuery= (conn, query) => {
//     return new Promise((resolve, reject) => {
//         conn.query(query, (err, res) => {
//             if (err) reject(err);
//             else resolve(res);
//         });
//     });
// };

router.get("/getDataEmployee", async (req,res) => {
    let token = req.query.token;
    let dataUser = jwt.decode(token, 'user');
    let department = "";
    let position = "";
    if(dataUser.position == "admin") position = "Manager";
    else if(dataUser.position == "Manager"){
        position = "Staff";
        department = dataUser.department;
    }

    let promisifydbconnection = promisify(pool.query).bind(pool);
    let result = await promisifydbconnection(
        `select *
        from 
        (SELECT u.kode_user "kode_user", u.name "name", j.department "department", j.position "position", count(a.kode_user) "jumlah_terlambat" 
        FROM user u 
        inner join jabatan j on u.kode_user = j.kode_user and j.department like "%${department}%" and j.position="${position}"
        left join absensi a on u.kode_user = a.kode_user and a.status_terlambat=1 
        group by u.kode_user
        ) qwe, 
        
        (SELECT u.kode_user "kode_user2", count(a.kode_user) "jumlah_izin" 
         FROM user u 
         inner join jabatan j on u.kode_user = j.kode_user and j.department like "%${department}%" and j.position="${position}"
         left join absensi a on u.kode_user = a.kode_user and (a.status_pulang_cepat=1 || (a.status_pulang_cepat=-1 and substr(a.absent_out,1,2)<=17)) 
         group by u.kode_user
        ) zxc
        where qwe.kode_user=zxc.kode_user2 and qwe.kode_user NOT IN
        (
            select kode_user 
            from m_evaluation
            where tahun = '${new Date().getFullYear()}'
        )`);
    res.json([{ 
        hasil:result
    }]);
})

router.post("/evaluateEmployee", async (req,res) => {
    let kode_user = req.body.kode_user;
    let nilai_absen = req.body.nilai_absen;
    let nilai_performance = req.body.nilai_performance;

    var today = new Date();
    var dd = String(today.getDate()).padStart(2, '0');
    var mm = String(today.getMonth() + 1).padStart(2, '0'); //January is 0!
    var yyyy = today.getFullYear();
    
    today = yyyy + '-' + mm + '-' + dd;
    let promisifydbconnection = promisify(pool.query).bind(pool);
    let result = await promisifydbconnection( 
        `INSERT INTO m_evaluation
        VALUES(null, ${kode_user}, '${new Date().getFullYear()}', '${nilai_absen}', '${nilai_performance}')`)

    let result2 = await promisifydbconnection(
    `INSERT INTO notifikasi
    VALUES(null, ${kode_user},'${"evaluation"}', '${nilai_absen}', '${nilai_performance}','' ,STR_TO_DATE('${today}', '%Y-%m-%d') ,0,'' )`)
    res.json([{ pesan:"Berhasil Evaluate Employee!!!" }]);
})

module.exports = router;