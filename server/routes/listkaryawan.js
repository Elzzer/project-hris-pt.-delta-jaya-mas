const express = require("express");
const config = require('./config');
const mysql= require('mysql');
const app= express();
const pool= mysql.createPool(config.database);
const jwt= require('jwt-simple');
const upload = require('./upload');
const fs= require('fs');

const router = express.Router();
const { promisify } = require('util');

// const getConnection= () => {
//     return new Promise((resolve, reject) => {
//         pool.getConnection((err, conn) => {
//             if (err) reject(err);
//             else resolve(conn);
//         });
//     });
// };

// const executeQuery= (conn, query) => {
//     return new Promise((resolve, reject) => {
//         conn.query(query, (err, res) => {
//             if (err) reject(err);
//             else resolve(res);
//         });
//     });
// };

router.get("/getDataEmployee", async (req,res) => {
    let token = req.query.token;
    let dataUser = jwt.decode(token, 'user');
    
    let promisfydbconnection = promisify(pool.query).bind(pool);
    result = await promisfydbconnection( 
        `SELECT u.kode_user "kode_user", u.name "name", u.address "address", DATE_FORMAT(CAST(u.birth_date AS CHAR), '%d %M %Y') "birth_date", DATE_FORMAT(CAST(u.birth_date AS CHAR), '%d %m %Y') "birth_date_age", u.gender "gender", u.phone "phone", u.status_kerja "status_kerja", u.status "status", u.city "city", u.education "education"
        , u.jurusan_sekolah "jurusan_sekolah", u.nama_sekolah "nama_sekolah", u.nik "nik", u.nokaryawan "nokaryawan", u.npwp "npwp", u.nobca "nobca", u.nokesehatan "nokesehatan", u.noketenagakerjaan "noketenagakerjaan", DATE_FORMAT(CAST(u.tglkerja AS CHAR), '%d %M %Y') "tglkerja", u.status_karyawan "status_karyawan", DATE_FORMAT(CAST(u.tglkerja AS CHAR), '%d %m %Y') "lamakerja"
        , j.position "position", j.department "department" , f.split1 "split1", f.split2 "split2"
        , g.basic_salary "basic_salary", DATE_FORMAT(CAST(u.birth_date AS CHAR), '%Y-%m-%d') "birth_date_update"
        FROM user u
        left join jabatan j on u.kode_user = j.kode_user
        left join foto f on u.kode_user = f.id_pemilik and f.jenis = 'foto_profil'
        left join gaji g on u.kode_user = g.kode_user
        where u.phone!='${dataUser.phone}'`);
    res.json([{hasil:result}]);
})

router.post("/updateStatus", async (req,res) => {
    let status_kerja = req.body.status_kerja;
    let kode_user = req.body.kode_user;

    //console.log(status_kerja + " | " + kode_user);
    let promisfydbconnection = promisify(pool.query).bind(pool);
    let result = await promisfydbconnection( `UPDATE user set status_kerja=${status_kerja} where kode_user=${kode_user}`);
    res.json([{pesan:"Berhasil Update Status!!!"}]);
})

router.post("/deleteUser", async (req,res) => {
    let kode = req.body.kode;
    let promisfydbconnection = promisify(pool.query).bind(pool);

    //DELETE USER
    let result = await promisfydbconnection(
        `DELETE FROM USER
        WHERE kode_user = ${kode}
        `);
    result = await promisfydbconnection(
        `DELETE FROM jabatan
        WHERE kode_user = ${kode}
        `);
    result = await promisfydbconnection(
        `DELETE FROM foto
        WHERE id_pemilik = ${kode}
        `);
    result = await promisfydbconnection(
        `DELETE FROM gaji
        WHERE kode_user = ${kode}
        `);
    
    res.json([{pesan:"Berhasil Delete User!!!"}]);
})



//UPDATE PROFILE
router.post("/updateProfile", upload.single("image"), async (req,res) => {
    let name = req.body.name
    let address = req.body.address;
    let birth_date = req.body.birthDate;
    let city = req.body.city;
    let status = req.body.status;
    let gender = req.body.gender;
    let token = req.body.token;
    let dataUser = jwt.decode(token, 'user');
    //console.log(birth_date);
    let promisfydbconnection = promisify(pool.query).bind(pool);
    let profil_picture=req.file;
    let query = await promisfydbconnection(
        `UPDATE USER
        set name='${name}', address='${address}', birth_date='${birth_date}', city='${city}', status='${status}', gender='${gender}'
        where phone = '${dataUser.phone}'`);
    if(profil_picture){
        fs.readFile(profil_picture.path, async function(err, data) {
            if (err) throw err;
            var encodedImage = Buffer.from(data).toString('base64');
            if(encodedImage.length<1500000){
                let split=profil_picture.filename.split(".");
                if(split[1].toUpperCase()=="JPEG" || split[1].toUpperCase()=="JPG" || split[1].toUpperCase()=="PNG"){
                    let result = await promisfydbconnection(
                        `SELECT f.id_foto "id_foto"
                        FROM user u, foto f 
                        where u.phone = '${dataUser.phone}' and u.kode_user=f.id_pemilik and f.jenis='foto_profil'`);
                    let query = await promisfydbconnection(
                        `UPDATE FOTO
                        set split1='${encodedImage.substring(0,800000)}', split2='${encodedImage.substring(800000)}'
                        where id_foto = '${result[0].id_foto}'`);
    
                    fs.unlinkSync(profil_picture.path);
                    let kembalian = [
                        {pesan:"Success Edit Profil"}
                    ];
                    return res.json(kembalian);
                }
                else{
                    fs.unlinkSync(profil_picture.path);
                    let kembalian = [
                        {pesan:"File yang diupload bukan foto"}
                    ];
                    return res.json(kembalian);
                }
            }
            else{
                fs.unlinkSync(profil_picture.path);
                let kembalian = [
                    {pesan:"Ukuran Foto Lebih Dari 1 MB"}
                ];
                return res.json(kembalian);
            }
        });
    }
    else{
        let kembalian = [
            {pesan:"Success Edit Profil"}
        ];
        return res.json(kembalian);
    }
})


//UPDATE PROFILE LISTKARYAWAN
router.post("/updateProfileKaryawan", upload.single("image"), async (req,res) => {
    let token = req.body.token;
    let dataUser = jwt.decode(token, 'user');

    let kode_user = req.body.kode_user;
    let name = req.body.name;

    let address = req.body.address;
    let birth_date = req.body.birthDate;
    let city = req.body.city;
    let phone = req.body.phone;
    let education = req.body.education;
    let department = req.body.department;
    let position = req.body.position;

    let status = req.body.status;
    let gender = req.body.gender;

    let jurusan_sekolah = req.body.jurusanSekolah;
    let nama_sekolah = req.body.namaSekolah;
    let nik = req.body.nik;
    let no_karyawan = req.body.noKaryawan;
    let npwp = req.body.npwp;
    let no_bca = req.body.noBca;
    let no_kesehatan = req.body.noKesehatan;
    let no_ketenagakerjaan = req.body.noKetenagakerjaan;
    let status_karyawan = req.body.statusKaryawan

    let promisfydbconnection = promisify(pool.query).bind(pool);
    let profil_picture=req.file;
    let query = await promisfydbconnection(
        `UPDATE USER
        set name='${name}'
            , address='${address}', birth_date='${birth_date}', city='${city}', phone='${phone}', education='${education}'
            , status='${status}', gender='${gender}'
            , jurusan_sekolah='${jurusan_sekolah}', nama_sekolah='${nama_sekolah}', nik='${nik}', nokaryawan='${no_karyawan}', npwp='${npwp}', nobca='${no_bca}'
            , nokesehatan='${no_kesehatan}', noketenagakerjaan='${no_ketenagakerjaan}', status_karyawan='${status_karyawan}'
        where kode_user = '${kode_user}'`);

    query = await promisfydbconnection(
        `UPDATE jabatan
        set department='${department}', position='${position}'
        where kode_user = '${kode_user}'`);
    if(profil_picture){
        fs.readFile(profil_picture.path, async function(err, data) {
            if (err) throw err;
            var encodedImage = Buffer.from(data).toString('base64');
            if(encodedImage.length<1500000){
                let split=profil_picture.filename.split(".");
                if(split[1].toUpperCase()=="JPEG" || split[1].toUpperCase()=="JPG" || split[1].toUpperCase()=="PNG"){
                    let result = await promisfydbconnection(
                        `SELECT f.id_foto "id_foto"
                        FROM user u, foto f 
                        where u.phone = '${dataUser.phone}' and u.kode_user=f.id_pemilik and f.jenis='foto_profil'`);
                    let query = await promisfydbconnection(
                        `UPDATE FOTO
                        set split1='${encodedImage.substring(0,800000)}', split2='${encodedImage.substring(800000)}'
                        where id_foto = '${result[0].id_foto}'`);
    
                    fs.unlinkSync(profil_picture.path);
                    let kembalian = [
                        {pesan:"Success Edit Profil"}
                    ];
                    return res.json(kembalian);
                }
                else{
                    fs.unlinkSync(profil_picture.path);
                    let kembalian = [
                        {pesan:"File yang diupload bukan foto"}
                    ];
                    return res.json(kembalian);
                }
            }
            else{
                fs.unlinkSync(profil_picture.path);
                let kembalian = [
                    {pesan:"Ukuran Foto Lebih Dari 1 MB"}
                ];
                return res.json(kembalian);
            }
        });
    }
    else{
        let kembalian = [
            {pesan:"Success Edit Profil"}
        ];
        return res.json(kembalian);
    }
})
module.exports = router;