const express = require("express");
const config = require('./config');
const mysql= require('mysql');
const app = express();
const pool= mysql.createPool(config.database);
const upload = require('./upload');
const fs= require('fs');
const jwt= require('jwt-simple');

const router = express.Router();
const { promisify } = require('util');

// const getConnection= () => {
//     return new Promise((resolve, reject) => {
//         pool.getConnection((err, conn) => {
//             if (err) reject(err);
//             else resolve(conn);
//         });
//     });
// };

// const executeQuery= (conn, query) => {
//     return new Promise((resolve, reject) => {
//         conn.query(query, (err, res) => {
//             if (err) reject(err);
//             else resolve(res);
//         });
//     });
// };

// router.get("/getDataPayroll", async (req,res) => {
//     let promisifydbconnection = promisify(pool.query).bind(pool);

//     ////UNTUK HITUNG TOTAL SALARY SEMUA (DI TEXTBOX)
//     // let result = await executeQuery(conn, 
//     //     `select (sum(g.basic_salary) + sum(g.transport_allowance) + sum(g.medical_allowance) + sum(g.position_allowance) - sum(mk.nominal)) "total_salary"
//     //     from user u
//     //     left join gaji g on u.kode_user = g.kode_user
//     //     left join m_keuangan mk on mk.kode_user = u.kode_user`)


//     //UNTUK HITUNG TOTAL SALARY SEMUA (DI TEXTBOX)
//     let result = await promisifydbconnection(
//         `select (sum(g.basic_salary) + sum(g.transport_allowance) + sum(g.medical_allowance) + sum(g.position_allowance)) "total_salary"
//         from user u, gaji g
//         where u.kode_user = g.kode_user`)
//     let result2 = await promisifydbconnection( 
//         `select sum(mk.nominal) "total_salary"
//         from m_keuangan mk
//         where mk.status=0`)
    

//     //UNTUK LIST SALARY EMPLOYEE
//     let result3 = await promisifydbconnection(
//         `select u.kode_user "kode_user", u.name "name"
//                 , (g.basic_salary + g.transport_allowance + g.medical_allowance + g.position_allowance + (case when sum(mk.nominal) IS NULL then 0 else sum(mk.nominal) end) ) "salary"
//                 , (case when sum(mk.nominal) IS NULL then 0 else sum(mk.nominal) end) "additional_min"
//                 , j.department "department", j.position "position"
//         from user u
//         left join gaji g on u.kode_user = g.kode_user
//         left join m_keuangan mk on mk.kode_user = u.kode_user and mk.status=0
//         left join jabatan j on u.kode_user = j.kode_user
//         group by u.kode_user`)
    
//     //PENGECEKAN SUDAH BAYAR BULAN INI ATAU TIDAK
//     let result4 = await promisifydbconnection( 
//         `SELECT max(tanggal) "tanggal"
//         FROM penggajian`)
//     //console.log(result4[0].bulan);
//     if(result4){
//         //console.log(new Date(result4[0].tanggal).getMonth().toString() + " | " + new Date().getMonth().toString() + " | " + new Date(result4[0].tanggal).getFullYear().toString() + " | " + new Date().getFullYear().toString());
//         if(new Date(result4[0].tanggal).getMonth().toString() == new Date().getMonth().toString() && new Date(result4[0].tanggal).getFullYear().toString() == new Date().getFullYear().toString()) result3 = [];
//     }

//     //ADD ALL KODE_NOTIFICATION
//     let result5 = await promisifydbconnection(`SELECT u.kode_user "kode_user", mk.id_keuangan "id_keuangan" from user u left join m_keuangan mk on u.kode_user = mk.kode_user and mk.status = 0`)

//     let hasilMerge = [];
//     let kodeNotif = [];
//     let tempKodeNotif = "";
//     for(let i=0; i<result3.length; i++){
//         tempKodeNotif = "";
//         for(let j=0; j<result5.length; j++){
//             //console.log(result3[i].kode_user + " " + result5[j].kode_user);
//             if(result3[i].kode_user == result5[j].kode_user){
//                 tempKodeNotif += (result5[j].id_keuangan + ",");
//             }
//         }
//         let temp = {
//             kode_user: result3[i].kode_user,
//             name: result3[i].name,
//             salary: result3[i].salary,
//             additional_min: result3[i].additional_min,
//             department: result3[i].department,
//             position: result3[i].position,
//             kode_mkeuangan: tempKodeNotif
//         };
//         hasilMerge.push(temp);
//     }
//     //console.log(hasilMerge);
//     //console.log(result3);

//     res.json([
//         {
//             total_salary:   (result[0].total_salary+result2[0].total_salary),
//             header_salary:  hasilMerge
//         }
//     ]);
// })

// router.get("/getDetailPayroll", async (req,res) => {
//     let kode_user = req.query.kode_user;
//     let promisifydbconnection = promisify(pool.query).bind(pool);
//     let result = await promisifydbconnection(
//         `select g.basic_salary "basic_salary", g.transport_allowance "transport_allowance", g.medical_allowance "medical_allowance", g.position_allowance "position_allowance"
//                 , mk.masalah "masalah", mk.nominal "nominal"
//                 , u.name "name"
//         from gaji g, m_keuangan mk, user u
//         where mk.kode_user = g.kode_user and mk.kode_user = u.kode_user and mk.kode_user = ${kode_user} and mk.status=0`);
    
//     if(result.length == 0){
//         result = await promisifydbconnection(
//             `select g.basic_salary "basic_salary", g.transport_allowance "transport_allowance", g.medical_allowance "medical_allowance", g.position_allowance "position_allowance"
//                     , u.name "name"
//             from gaji g, user u
//             where u.kode_user = g.kode_user and u.kode_user = ${kode_user}`);
//     }
//     res.json([
//         {
//             detail_salary: result
//         }
//     ]);
// })

// router.post("/insertSalary", async (req,res) => {
//     let hasil = req.body.hasil;
//     let notifikasi = req.body.notifikasi;
//     let promisifydbconnection = promisify(pool.query).bind(pool);

//     //INSERT DB PENGGAJIAN
//     let result = await promisifydbconnection(hasil);

//     //INSERT DB NOTIFIKASI
//     let result2 = await promisifydbconnection(notifikasi);

//     //UPDATE MANAGEMENT KEUANGAN
//     let result3 = await promisifydbconnection(`UPDATE m_keuangan set status=1 where status=0`)

//     res.json([{ pesan: "Insert Salary Berhasil!!!" }]);
// })


router.post("/multiUploadPenggajian", upload.single("excel"), async (req, res) => {
    const excelToJson = require('convert-excel-to-json');
    let excel=req.file;

    const resultexcel = excelToJson({
        source: fs.readFileSync(excel.path), // fs.readFileSync return a Buffer
        header:{
            rows: 2
        }
    });

    let promisfydbconnection = promisify(pool.query).bind(pool);
    let max_id = await promisfydbconnection(`select max(kode_user) "kode" from user`);
    max_id = max_id[0].kode;
    for (let i2 = 0; i2 < resultexcel.Sheet1.length; i2++) {
        let a = resultexcel.Sheet1[i2].A;//name
        let b = resultexcel.Sheet1[i2].B;//nokaryawan
        let c = resultexcel.Sheet1[i2].C;//status
        let d = resultexcel.Sheet1[i2].D;//gaji
        let e = resultexcel.Sheet1[i2].E;//department
        let f = resultexcel.Sheet1[i2].F;//bulan
        let g = resultexcel.Sheet1[i2].G;//tahun

        //insert penggajian
        result = await promisfydbconnection(
            `INSERT INTO penggajian
            VALUES(null, '${a}', '${b}', '${c}', ${d}, '${e}', '${f}', '${g}')`
        );
    }

    fs.unlinkSync(excel.path);
    res.json([{ pesan: "Insert Penggajian Berhasil!!!" }]);
})


module.exports = router;