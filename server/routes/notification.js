const express = require("express");
const config = require('./config');
const mysql= require('mysql');
const app = express();
const pool= mysql.createPool(config.database);
const jwt= require('jwt-simple');
const upload = require('./upload');
const fs= require('fs');

const router = express.Router();
const { promisify } = require('util');

// const getConnection= () => {
//     return new Promise((resolve, reject) => {
//         pool.getConnection((err, conn) => {
//             if (err) reject(err);
//             else resolve(conn);
//         });
//     });
// };

// const executeQuery= (conn, query) => {
//     return new Promise((resolve, reject) => {
//         conn.query(query, (err, res) => {
//             if (err) reject(err);
//             else resolve(res);
//         });
//     });
// };

router.get("/getcountnotification/:token", async (req, res) => {
    let token=req.params.token;
    let dataUser = jwt.decode(token, 'user');

    let promisifydbconnection = promisify(pool.query).bind(pool);
    result = await promisifydbconnection(`SELECT * FROM user where phone='${dataUser.phone}'`);
    
    result2 = await promisifydbconnection(`SELECT count(*) "jumlah" FROM notifikasi where kode_user=${result[0].kode_user} and status=0`);
    
    return res.json({
        jumlah:result2[0].jumlah
    });
});

router.get("/getdatanotification/:token", async (req, res) => {
    let token=req.params.token;
    let dataUser = jwt.decode(token, 'user');

    let promisifydbconnection = promisify(pool.query).bind(pool);
    result = await promisifydbconnection(`SELECT * FROM user where phone='${dataUser.phone}'`);
    
    result2 = await promisifydbconnection(`SELECT * FROM notifikasi where kode_user=${result[0].kode_user} order by tanggal desc`);
    //console.log(result2);
    return res.json({
        data:result2
    });
});

router.get("/getdatasalary/:kode_user/:id_keuangan", async (req, res) => {
    let kode_user=req.params.kode_user;
    let id_keuangan=req.params.id_keuangan;
    let arr_keuangan=[];

    let promisifydbconnection = promisify(pool.query).bind(pool);
    result = await promisifydbconnection(`SELECT * FROM user where kode_user='${kode_user}'`);

    result1 = await promisifydbconnection(`SELECT * FROM gaji where kode_user='${kode_user}'`);

    result2 = await promisifydbconnection(`SELECT * FROM m_keuangan where kode_user='${kode_user}' and status=1`);

    id_keuangan=id_keuangan.substr(0,id_keuangan.length-1);
    split=id_keuangan.split(",");
    if(split[0]!="null"){
        for (let i = 0; i < split.length; i++) {
            for (let j = 0; j < result2.length; j++) {
                if(split[i]==result2[j].id_keuangan){
                    arr_keuangan.push(
                        {
                            masalah:result2[j].masalah,
                            nominal:result2[j].nominal
                        }
                    );
                }
            }
        }
    }
    //console.log(arr_keuangan);
    return res.json({
        name:result[0].name,
        gaji:result1,
        keuangan:arr_keuangan
    });
});

router.get("/getdatasalary/:kode_user/:id_keuangan", async (req, res) => {
    let kode_user=req.params.kode_user;
    let id_keuangan=req.params.id_keuangan;
    let arr_keuangan=[];

    let promisifydbconnection = promisify(pool.query).bind(pool);
    result = await promisifydbconnection(`SELECT * FROM user where kode_user='${kode_user}'`);

    result1 = await promisifydbconnection(`SELECT * FROM gaji where kode_user='${kode_user}'`);

    result2 = await promisifydbconnection(`SELECT * FROM m_keuangan where kode_user='${kode_user}' and status=1`);

    id_keuangan=id_keuangan.substr(0,id_keuangan.length-1);
    split=id_keuangan.split(",");
    if(split[0]!="null"){
        for (let i = 0; i < split.length; i++) {
            for (let j = 0; j < result2.length; j++) {
                if(split[i]==result2[j].id_keuangan){
                    arr_keuangan.push(
                        {
                            masalah:result2[j].masalah,
                            nominal:result2[j].nominal
                        }
                    );
                }
            }
        }
    }
    //console.log(arr_keuangan);
    return res.json({
        name:result[0].name,
        gaji:result1,
        keuangan:arr_keuangan
    });
});

router.post("/changeReadStatus", async (req, res) => {
    let kode_notifikasi=req.body.kode_notifikasi;

    let promisifydbconnection = promisify(pool.query).bind(pool);
    result = await promisifydbconnection(`update notifikasi set status=1 where kode_notifikasi=${kode_notifikasi}`);

    return res.json({
        pesan:"berhasil read notifikasi"
    });
});

module.exports = router;