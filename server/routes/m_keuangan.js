const express = require("express");
const config = require('./config');
const mysql= require('mysql');
const app = express();
const pool= mysql.createPool(config.database);
const jwt= require('jwt-simple');
const upload = require('./upload');
const fs= require('fs');

const router = express.Router();
const { promisify } = require('util');

// const getConnection= () => {
//     return new Promise((resolve, reject) => {
//         pool.getConnection((err, conn) => {
//             if (err) reject(err);
//             else resolve(conn);
//         });
//     });
// };

// const executeQuery= (conn, query) => {
//     return new Promise((resolve, reject) => {
//         conn.query(query, (err, res) => {
//             if (err) reject(err);
//             else resolve(res);
//         });
//     });
// };

router.get("/getuser/:token", async (req, res) => {
    let token=req.params.token;
    let dataUser = jwt.decode(token, 'user');
    let promisifydbconnection = promisify(pool.query).bind(pool);
    result = await promisifydbconnection(`SELECT * FROM user where phone='${dataUser.phone}'`);
    return res.json({
        user:result[0]
    });
});

router.post("/submitkeuangan", upload.single("image"), async (req,res) => {
    let kode_user = req.body.kode_user
    let masalah = req.body.masalah;
    let tgl = req.body.tgl;
    let nominal = req.body.nominal;
    let bank = req.body.bank;

    let promisifydbconnection = promisify(pool.query).bind(pool);
    let profil_picture=req.file;
    if(profil_picture){
        fs.readFile(profil_picture.path, async function(err, data) {
            if (err) throw err;
            var encodedImage = Buffer.from(data).toString('base64');
            if(encodedImage.length<1500000){
                let split=profil_picture.filename.split(".");
                if(split[1].toUpperCase()=="JPEG" || split[1].toUpperCase()=="JPG" || split[1].toUpperCase()=="PNG"){
                    //insert foto
                    result1 = await promisifydbconnection(
                        `INSERT INTO foto
                        VALUES(null , ${kode_user}, '${"bukti_keuangan"}', '${split[1]}', '${encodedImage.substring(0,800000)}','${encodedImage.substring(800000)}')`
                    );

                    //cari max
                    let max_id = await promisifydbconnection(`select max(id_foto) "id_foto" from foto`);

                    //insert m_keuangan
                    result2 = await promisifydbconnection(
                        `INSERT INTO m_keuangan
                        VALUES(null , ${kode_user}, '${masalah}', '${tgl}', ${nominal}, '${bank}' , ${max_id[0].id_foto},0)`
                    );

                    fs.unlinkSync(profil_picture.path);
                    let kembalian = [
                        {pesan:"Success submit"}
                    ];
                    return res.json(kembalian);
                }
                else{
                    fs.unlinkSync(profil_picture.path);
                    let kembalian = [
                        {pesan:"File yang diupload bukan foto"}
                    ];
                    return res.json(kembalian);
                }
            }
            else{
                fs.unlinkSync(profil_picture.path);
                let kembalian = [
                    {pesan:"Ukuran Foto Lebih Dari 1 MB"}
                ];
                return res.json(kembalian);
            }
        });
    }
    else{
        let kembalian = [
            {pesan:"Belum Upload Bukti!"}
        ];
        return res.json(kembalian);
    }
})

module.exports = router;