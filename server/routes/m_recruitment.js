const express = require("express");
const config = require('./config');
const mysql= require('mysql');
const app = express();
const pool= mysql.createPool(config.database);
const jwt= require('jwt-simple');
const upload = require('./upload');
const fs= require('fs');

const router = express.Router();
const { promisify } = require('util');

// const getConnection= () => {
//     return new Promise((resolve, reject) => {
//         pool.getConnection((err, conn) => {
//             if (err) reject(err);
//             else resolve(conn);
//         });
//     });
// };

// const executeQuery= (conn, query) => {
//     return new Promise((resolve, reject) => {
//         conn.query(query, (err, res) => {
//             if (err) reject(err);
//             else resolve(res);
//         });
//     });
// };

router.get("/getrecruitment", async (req, res) => {
    let promisifydbconnection = promisify(pool.query).bind(pool);
    result = await promisifydbconnection(
    `SELECT * 
    FROM m_recruitment m, foto f, negotiations n, evaluation_skill e
    where m.id_foto=f.id_foto and m.id_negotiation=n.id_negotiation 
    and m.id_evaluation_skill=e.id_evaluation_skill`);
    return res.json({
        data:result
    });
});
router.get("/getrecruitment/:like", async (req, res) => {
    let like = req.params.like

    let promisifydbconnection = promisify(pool.query).bind(pool);
    result = await promisifydbconnection(
    `SELECT * 
    FROM m_recruitment m, foto f, negotiations n, evaluation_skill e
    where m.id_foto=f.id_foto and m.id_negotiation=n.id_negotiation 
    and m.id_evaluation_skill=e.id_evaluation_skill and m.name like '%${like}%'`);
    return res.json({
        data:result
    });
});

router.post("/submitrecruitperson", upload.single("image"), async (req,res) => {
    let name = req.body.name
    let address = req.body.address;
    let birthdate = req.body.birthdate;
    let city = req.body.city;
    let phone = req.body.phone;
    let education = req.body.education;
    let department = req.body.department;
    let position = req.body.position;
    let status = req.body.status;
    let gender = req.body.gender;

    let promisifydbconnection = promisify(pool.query).bind(pool);
    let profil_picture=req.file;
    if(profil_picture){
        fs.readFile(profil_picture.path, async function(err, data) {
            if (err) throw err;
            var encodedImage = Buffer.from(data).toString('base64');
            if(encodedImage.length<1500000){
                let split=profil_picture.filename.split(".");
                if(split[1].toUpperCase()=="JPEG" || split[1].toUpperCase()=="JPG" || split[1].toUpperCase()=="PNG"){
                    //insert evaluation_skill
                    result1 = await promisifydbconnection(
                        `INSERT INTO evaluation_skill
                        VALUES(null , '', '', '', '' ,'' ,'')`
                    );
                    let id_evaluation_skill = await promisifydbconnection(`select max(id_evaluation_skill) "id_evaluation_skill" from evaluation_skill`);

                    //insert negotiations
                    result2 = await promisifydbconnection(
                        `INSERT INTO negotiations
                        VALUES(null , '', '', '', '')`
                    );
                    let id_negotiation = await promisifydbconnection(`select max(id_negotiation) "id_negotiation" from negotiations`);
                    
                    //insert foto
                    result3 = await promisifydbconnection(
                        `INSERT INTO foto
                        VALUES(null , '', '${"profil_recruitment"}', '${split[1]}', '${encodedImage.substring(0,800000)}','${encodedImage.substring(800000)}')`
                    );
                    let id_foto = await promisifydbconnection(`select max(id_foto) "id_foto" from foto`);
                    
                    //insert m_recruitment
                    result4 = await promisifydbconnection(
                        `INSERT INTO m_recruitment
                        VALUES(null , '${name}', '${address}', '${birthdate}', '${city}', '${phone}' ,'${education}','${department}','${position}','${status}','${gender}', ${id_foto[0].id_foto}, ${id_evaluation_skill[0].id_evaluation_skill}, ${id_negotiation[0].id_negotiation},'')`
                    );

                    fs.unlinkSync(profil_picture.path);
                    let kembalian = [
                        {pesan:"Recruit Person Submitted!"}
                    ];
                    return res.json(kembalian);
                }
                else{
                    fs.unlinkSync(profil_picture.path);
                    let kembalian = [
                        {pesan:"File yang diupload bukan foto"}
                    ];
                    return res.json(kembalian);
                }
            }
            else{
                fs.unlinkSync(profil_picture.path);
                let kembalian = [
                    {pesan:"Ukuran Foto Lebih Dari 1 MB"}
                ];
                return res.json(kembalian);
            }
        });
    }
    else{
        let kembalian = [
            {pesan:"Belum Upload Foto!"}
        ];
        return res.json(kembalian);
    }
})

router.post("/update_evaluation_skill", async (req, res) => {
    let id_evaluation_skill=req.body.id_evaluation_skill;
    let experience=req.body.experience;
    let project=req.body.project;
    let teamwork=req.body.teamwork;
    let hard_skill=req.body.hard_skill;
    let soft_skill=req.body.soft_skill;
    let note_evaluation_skill=req.body.note_evaluation_skill;

    let promisifydbconnection = promisify(pool.query).bind(pool);
    result = await promisifydbconnection(
        `update evaluation_skill set 
        experience='${experience}',
        project='${project}',
        teamwork='${teamwork}',
        hard_skill='${hard_skill}',
        soft_skill='${soft_skill}',
        note_evaluation_skill='${note_evaluation_skill}'
        where id_evaluation_skill=${id_evaluation_skill}`
    );

    return res.json({
        pesan:"berhasil save evaluation skill"
    });
});

router.post("/update_negotiation", async (req, res) => {
    let id_negotiation=req.body.id_negotiation;
    let contract=req.body.contract;
    let requirement=req.body.requirement;
    let salary=req.body.salary;
    let note_negotiation=req.body.note_negotiation;

    let promisifydbconnection = promisify(pool.query).bind(pool);
    result = await promisifydbconnection(
        `update negotiations set 
        contract='${contract}',
        requirement='${requirement}',
        salary=${salary},
        note_negotiation='${note_negotiation}'
        where id_negotiation=${id_negotiation}`
    );

    return res.json({
        pesan:"berhasil save negotiation"
    });
});

router.post("/update_hasil", async (req, res) => {
    let kode_recruitment=req.body.kode_recruitment;
    let hasil=req.body.hasil;
    //console.log(kode_recruitment);
    let promisifydbconnection = promisify(pool.query).bind(pool);
    result = await promisifydbconnection(
        `update m_recruitment set 
        diterima='${hasil}'
        where kode_recruitment=${kode_recruitment}`
    );

    return res.json({
        pesan:`berhasil ${hasil} Recruitment`
    });
});

module.exports = router;