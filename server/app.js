﻿const createError = require("http-errors");
const express = require("express");
const path = require("path");
const cookieParser = require("cookie-parser");	
const logger = require("morgan");

const indexRouter = require("./routes/loginregister");
const absensi = require("./routes/absensi");
const listkaryawan = require("./routes/listkaryawan");
const m_evaluation = require("./routes/m_evaluation");
const m_keuangan = require("./routes/m_keuangan");
const m_penggajian = require("./routes/m_penggajian");
const m_recruitment = require("./routes/m_recruitment");
const notification = require("./routes/notification");
const laporan = require("./routes/laporan");

const app = express();

app.use(logger("dev"));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.resolve(__dirname, "build")));

app.use("/api", indexRouter);
app.use("/absen", absensi);
app.use("/listkaryawan", listkaryawan);
app.use("/m_evaluation", m_evaluation);
app.use("/m_keuangan", m_keuangan);
app.use("/m_penggajian", m_penggajian);
app.use("/m_recruitment", m_recruitment);
app.use("/notification", notification);
app.use("/laporan", laporan);

app.get("*", (req, res) => {
  res.sendFile("build/index.html", { root: __dirname });
});

// catch 404 and forward to error handler
app.use((req, res, next) => {
  next(createError(404));
});

// TODO Web Template Studio: Add your own error handler here.
if (process.env.NODE_ENV === "production") {
  // Do not send stack trace of error message when in production
  app.use((err, req, res, next) => {
    res.status(err.status || 500);
    res.send("Error occurred while handling the request.");
  });
} else {
  // Log stack trace of error message while in development
  app.use((err, req, res, next) => {
    res.status(err.status || 500);
    console.log(err);
    res.send(err.message);
  });
}

module.exports = app;
