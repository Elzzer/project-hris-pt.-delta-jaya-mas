﻿import React from "react";
import { Switch, Route } from "react-router-dom";
import "./App.css";
import NavBar from "./components/NavBar";
import Footer from "./components/Footer";

import 'bootstrap/dist/css/bootstrap.min.css';
import Login from "./components/Login"
import Register from "./components/Register"
import AbsentOut from "./components/AbsentOut"
import Home from "./components/Home"
import ListKaryawan from "./components/ListKaryawan"
import EditProfile from "./components/EditProfile"
import ManagementPenggajian from "./components/ManagementPenggajian"
import ManagementRecruitment from "./components/ManagementRecruitment"
import RecruitPerson from "./components/RecruitPerson"
import LaporanKeuangan from "./components/LaporanKeuangan"

//TODO Web Template Studio: Add routes for your new pages here.
const App = () => {
    return (
      <React.Fragment>
				  <Route exact path = "/" component = { Login } />
				  <Route exact path = "/register" component = { Register } />
				  <Route exact path = "/absentOut" component = { AbsentOut } />
				  <Route exact path = "/home" component = { Home } />
				  <Route exact path = "/listkaryawan" component = { ListKaryawan } />
				  <Route exact path = "/editprofile" component = { EditProfile } />
				  
				  <Route exact path = "/penggajian" component = { ManagementPenggajian } />

				  <Route exact path = "/recruitment" component = { ManagementRecruitment } />
				  <Route exact path = "/recruitperson" component = { RecruitPerson } />
				  
				  <Route exact path = "/laporan" component = { LaporanKeuangan } />
      </React.Fragment>
    );
}

export default App;
