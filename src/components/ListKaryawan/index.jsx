import React, { useState, useEffect } from 'react';
import { useHistory } from 'react-router-dom';
import { MDBDataTableV5 } from 'mdbreact';
import DetailEmployee from "./detailEmployee"
import EditEmployee from "./editEmployee"
import NavBar from "../NavBar/";
import ReactHTMLTableToExcel from 'react-html-table-to-excel';

const ListKaryawan = () => {
    const history= useHistory();
    const token = window.sessionStorage.getItem("token")
    const [ dataEmployee, setDataEmployee ] = useState([]);

    //FOR TABLE
    const [ dataTable, setDataTable ]= useState();
    const [ dataSetTable, setDataSetTable] = useState();

    //FOR DETAIL POPUP
    const [ dataDetailEmployee, setDataDetailEmployee ] = useState([]); //FOR POP UP DETAIL 
    const [isModal, setIsModal] = useState(false);

    //FOR EDIT POPUP
    const [ dataEditEmployee, setDataEditEmployee ] = useState([]); //FOR POP UP EDIT 
    const [ isModalEdit, setIsModalEdit ] = useState(false);

    async function getDataEmployee(){
        try {
            const res = await fetch(`/listkaryawan/getDataEmployee?token=${token}`, { 
                method : "GET",
                headers : { "Content-Type" : "application/json" }
            });
            let a = await res.json();
            setDataEmployee(a[0].hasil);
            setAllTable(a[0].hasil);
        } catch (error) {
            console.error(error);
        }
    }

    useEffect(() => {
        getDataEmployee();
    }, []);

    function setAllTable(temp){
        let tempTable = temp.map((item, index) => {
            let param_status_kerja = "";
            let class_btn_delete = "";
            if(item.status_kerja == 1) {
                param_status_kerja = "Disable";
                class_btn_delete = "btn btn-danger"
            }
            else if(item.status_kerja == 0) {
                param_status_kerja = "Enable";
                class_btn_delete = "btn btn-success";
            }
            return ({
                row: index+1,
                name: item.name,
                phone: item.phone,
                address: item.address,
                department: item.department,
                position: item.position,

                detail: <button onClick={() => handleShowPopupDetail(item) } 
                        style={{fontSize:"0.7vw", textAlign:"center"}}
                        className="btn btn-primary">
                        &nbsp; Detail &nbsp;
                        </button>,
                delete: <button onClick={() => handleDeleteEmployee(param_status_kerja,item.kode_user) } 
                        style={{fontSize:"0.68vw", textAlign:"center"}}
                        className={class_btn_delete}>
                        &nbsp; {param_status_kerja} &nbsp;
                        </button>,
                hapus: <button onClick={() => handleHapusEmployee(item.kode_user) } 
                        style={{fontSize:"0.50vw"}}
                        className="btn btn-danger">
                        <img src="https://img.icons8.com/small/16/000000/trash--v1.png"/>
                        </button>,
                edit:   <button onClick={() => handleEditEmployee(item) } 
                        style={{fontSize:"0.50vw"}}
                        className="btn btn-success">     
                        <img src="https://img.icons8.com/small/16/000000/edit.png"/>
                        </button>
            })
        });
        setDataSetTable(tempTable);


        setDataTable({
            columns:  [
                { label: '#', field: 'row' },
                { label: 'EMPLOYEE', field: 'name' },
                { label: 'PHONE', field: 'phone' },
                { label: 'ADDRESS', field: 'address' },
                { label: 'DEPARTMENT', field: 'department' },
                { label: 'POSITION', field: 'position' },
                { label: 'DETAIL', field: 'detail' },
                { label: 'STATUS', field: 'delete' },
                { label: 'DELETE', field: 'hapus' },
                { label: 'EDIT', field: 'edit' }
            ],
            rows: tempTable
        });
    }

    function handleShowPopupDetail(item){
        setDataDetailEmployee(item);
        setIsModal(true);
    }

    async function handleDeleteEmployee(param, kode){
        let temp_status_kerja = -1;
        if(param == "Disable") temp_status_kerja = 0;
        else if(param == "Enable") temp_status_kerja = 1;
        let obj= {
            status_kerja: temp_status_kerja,
            kode_user: kode
        };
        try {
            const res = await fetch("/listkaryawan/updateStatus", { 
                method : "POST",
                headers : { "Content-Type" : "application/json" },
                body : JSON.stringify(obj)
            });
            let a = await res.json();
            alert(a[0].pesan);
            getDataEmployee();
        } catch (error) {
            console.error(error);
        }
    }
    function handleCloseDetail(){
        setIsModal(false);
    }

    async function handleHapusEmployee(kode){
        let obj= {
            kode: kode
        };
        try {
            const res = await fetch("/listkaryawan/deleteUser", { 
                method : "POST",
                headers : { "Content-Type" : "application/json" },
                body : JSON.stringify(obj)
            });
            let a = await res.json();
            alert(a[0].pesan);
            getDataEmployee();
        } catch (error) {
            console.error(error);
        }
    }

    async function handleEditEmployee(item){
        setDataEditEmployee(item);
        setIsModalEdit(true);
    }
    function handleCloseEdit(){
        setIsModalEdit(false);
    }

    function print() {
        window.print()
    }
    return ( 
        <>
            <br/>
                <button onClick={print} style={{right:0,position:"absolute",marginRight:"15%"}}>Print<img src="https://img.icons8.com/carbon-copy/30/000000/print.png"/></button>
            <br/><br/>
            <NavBar/>
            <div className='row' style={{width:"100%",fontFamily:"arial"}}>
                <div className="col-md-2"></div>
                <div className="col-md-8">
                    { dataTable && dataTable.rows.length ? 
                        <>
                            <ReactHTMLTableToExcel
                            id="test-table-xls-button"
                            className="download-table-xls-button"
                            table="table-to-xls"
                            filename="tablexls"
                            sheet="tablexls"
                            buttonText="Download as XLS"/>
                            <MDBDataTableV5 responsive
                                            striped
                                            searchBottom={ false }
                                            data={ dataTable } 
                                            id="table-to-xls"/>
                        </>
                    
                    : 'No Employee.' }
                </div>
                <div className="col-md-2"></div>
            </div>
            <DetailEmployee onClose= {handleCloseDetail} dataDetailEmployee={dataDetailEmployee} isModal={isModal} />
            <EditEmployee onClose= {handleCloseEdit} dataEditEmployee={dataEditEmployee} isModalEdit={isModalEdit} />
            {/* <button onClick={() => history.push("/home")}>Back</button> */}
        </>
     );
}
 
export default ListKaryawan;