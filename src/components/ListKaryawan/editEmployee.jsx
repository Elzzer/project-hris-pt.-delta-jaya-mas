import React, { Component, useState, useEffect } from 'react';
import Modal from 'react-modal';

const EditEmployee = ({onClose, dataEditEmployee, isModalEdit}) => {
    const token = window.sessionStorage.getItem("token");
    
    //FIELD EDIT PROFILE
    const [image, setImage] = useState("");
    const [fullname, setFullname] = useState("");

    const [address, setAddress] = useState("");
    const [birthDate, setBirthDate] = useState("");
    const [city, setCity] = useState("");
    const [phone, setPhone] = useState("");
    const [education, setEducation] = useState("");
    const [department, setDepartment] = useState("");
    const [position, setPosition] = useState("");

    const [status, setStatus] = useState("");
    const [gender, setGender] = useState("");

    const [jurusanSekolah, setJurusanSekolah] = useState("");
    const [namaSekolah, setNamaSekolah] = useState("");
    const [nik, setNik] = useState("");
    const [noKaryawan, setNoKaryawan] = useState("");
    const [npwp, setNpwp] = useState("");
    const [noBca, setNoBca] = useState("");
    const [noKesehatan, setNoKesehatan] = useState("");
    const [noKetenagakerjaan, setNoKetenagakerjaan] = useState("");
    const [statusKaryawan, setStatusKaryawan] = useState("");

    function setStateAll(){
        setFullname(dataEditEmployee.name);

        setAddress(dataEditEmployee.address);
        setBirthDate(dataEditEmployee.birth_date_update);
        setCity(dataEditEmployee.city);
        setPhone(dataEditEmployee.phone);
        setEducation(dataEditEmployee.education);
        setDepartment(dataEditEmployee.department);
        setPosition(dataEditEmployee.position);

        setStatus(dataEditEmployee.status);
        setGender(dataEditEmployee.gender);
        
        setJurusanSekolah(dataEditEmployee.jurusan_sekolah);
        setNamaSekolah(dataEditEmployee.nama_sekolah);
        setNik(dataEditEmployee.nik);
        setNoKaryawan(dataEditEmployee.nokaryawan);
        setNpwp(dataEditEmployee.npwp);
        setNoBca(dataEditEmployee.nobca);
        setNoKesehatan(dataEditEmployee.nokesehatan);
        setNoKetenagakerjaan(dataEditEmployee.noketenagakerjaan);
        setStatusKaryawan(dataEditEmployee.status_karyawan);
    }
    useEffect(() => {
        setStateAll();
    }, [dataEditEmployee]);

    function handleClosePopup(e){
        e.preventDefault();
        onClose();
    }
    async function processEditProfile(e){
        e.preventDefault();
        const formData= new FormData();
        formData.append('kode_user', dataEditEmployee.kode_user);
        formData.append('image',image);
        formData.append('name', fullname);

        formData.append('address',address);
        formData.append('birthDate',birthDate);
        formData.append('city',city);
        formData.append('phone',phone);
        formData.append('education',education);
        formData.append('department',department);
        formData.append('position',position);

        formData.append('status',status);
        formData.append('gender',gender);
        
        formData.append('jurusanSekolah',jurusanSekolah);
        formData.append('namaSekolah',namaSekolah);
        formData.append('nik',nik);
        formData.append('noKaryawan',noKaryawan);
        formData.append('npwp',npwp);
        formData.append('noBca',noBca);
        formData.append('noKesehatan',noKesehatan);
        formData.append('noKetenagakerjaan',noKetenagakerjaan);
        formData.append('statusKaryawan',statusKaryawan);

        formData.append('token',token);
        try {
            const res = await fetch("/listkaryawan/updateProfileKaryawan", { 
                method : "POST",
                body : formData
            });
            let a = await res.json();
            alert(a[0].pesan);
        } catch (error) {
            console.error(error);
        }
    }
    return(
        <>
            <Modal isOpen={isModalEdit} onRequestClose={handleClosePopup} style={{overlay:{backgroundColor:'gray'},content:{color:'black', marginLeft:"auto", marginRight:"auto"}}}>
                <center>
                    <h1>Edit Profile</h1>
                    <img src={"data:image/png;base64, "+dataEditEmployee.split1+dataEditEmployee.split2} style={{width:"200px", height:"200px"}}/><br/>
                    <input type="file" name="image" id="image" onChange={ e => setImage(e.target.files[0]) }/>
                </center><br/>
                <form onSubmit={processEditProfile}>
                    <div className='row' style={{width:"100%",fontFamily:"arial"}}>
                        <div className="col-md-4"></div>
                        <div className="col-md-8" style={{textAlign:"left"}}>
                                <label>Full Name                :  </label><input type="text" name="fullname"      value={fullname}                onChange={(e) => setFullname(e.target.value)}           />  <br/>

                                <label>Address                  :  </label><input type="text" name="address"       value={address}             onChange={(e) => setAddress(e.target.value)}            />  <br/>
                                <label>Birth Date               :  </label><input type="date" name="birth_date"    defaultValue={birthDate}   onChange={(e) => setBirthDate(e.target.value)}          />  <br/>
                                <label>City                     :  </label><input type="text" name="city"          value={city}                onChange={(e) => setCity(e.target.value)}               />  <br/>
                                <label>Phone                    :  </label><input type="text" name="phone"         value={phone}               onChange={(e) => setPhone(e.target.value)}              />  <br/>
                                <label>Education                :  </label><input type="text" name="education"     value={education}           onChange={(e) => setEducation(e.target.value)}          />  <br/>
                                <label>Department               :  </label><input type="text" name="department"    value={department}          onChange={(e) => setDepartment(e.target.value)}         />  <br/>
                                <label>Position                 :  </label><input type="text" name="position"      value={position}            onChange={(e) => setPosition(e.target.value)}           />  <br/>
                                <label>Status                   :  </label><select            name="status"                                    onChange={(e) => setStatus(e.target.value)}>
                                        {status == "Single" ?               <option value="Single"              selected>Single             </option> : <option value="Single">             Single              </option>}
                                        {status == "In a Relationship" ?    <option value="In a Relationship"   selected>In a Relationship  </option> : <option value="In a Relationship">  In a Relationship   </option>}
                                        {status == "Married" ?              <option value="Married"             selected>Married            </option> : <option value="Married">            Married             </option>}
                                        {status == "Widow / Widowed" ?      <option value="Widow / Widowed"     selected>Widow / Widowed    </option> : <option value="Widow / Widowed">    Widow / Widowed     </option>}
                                    </select> <br/>
                                <label>Gender                   :  </label><select            name="gender"                                    onChange={(e) => setGender(e.target.value)}>
                                        {gender == "Male" ?      <option value="Male"    selected>Male   </option> : <option value="Male">   Male    </option>}
                                        {gender == "Female" ?    <option value="Female"  selected>Female </option> : <option value="Female"> Female  </option>}
                                    </select>    <br/>
                                <label>Jurusan Sekolah          :  </label><input type="text" name=""         value={jurusanSekolah}   onChange={(e) => setJurusanSekolah(e.target.value)}        />                                                                                                                  <br/>  
                                <label>Nama Sekolah             :  </label><input type="text" name=""         value={namaSekolah}      onChange={(e) => setNamaSekolah(e.target.value)}           />                                                                                                                  <br/>
                                <label>NIK                      :  </label><input type="text" name=""         value={nik}               onChange={(e) => setNik(e.target.value)}                   />                                                                                                                  <br/>
                                <label>No Karyawan              :  </label><input type="text" name=""         value={noKaryawan}        onChange={(e) => setNoKaryawan(e.target.value)}            />                                                                                                                  <br/>
                                <label>NPWP                     :  </label><input type="text" name=""         value={npwp}              onChange={(e) => setNpwp(e.target.value)}                  />                                                                                                                   <br/>
                                <label>No Rekening BCA          :  </label><input type="text" name=""         value={noBca}             onChange={(e) => setNoBca(e.target.value)}                 />                                                                                                                 <br/>
                                <label>No BPJS Kesehatan        :  </label><input type="text" name=""         value={noKesehatan}       onChange={(e) => setNoKesehatan(e.target.value)}           />                                                                                                                 <br/>
                                <label>No BPJS Ketenagakerjaan  :  </label><input type="text" name=""         value={noKetenagakerjaan} onChange={(e) => setNoKetenagakerjaan(e.target.value)}     />                                                                                                                <br/>
                                <label>Status Karyawan          :  </label><input type="text" name=""         value={statusKaryawan}   onChange={(e) => setStatusKaryawan(e.target.value)}        />                                                                                                                   <br/>
                                
                        </div>
                    </div>
                    <br/>
                    <center>
                        <input type="submit" value="update"/>
                    </center>
                </form>
            </Modal>
        </>
    );
}
export default EditEmployee;