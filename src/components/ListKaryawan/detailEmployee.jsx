import React, { Component, useState } from 'react';
import Modal from 'react-modal';

const DetailEmployee = ({onClose, dataDetailEmployee, isModal}) => {

    function handleClosePopup(e){
        e.preventDefault();
        onClose();
    }

    function countAge(item){
        let temp = item.split(" ");
        let dateNow = parseInt(new Date().getDate());
        let monthNow = parseInt(new Date().getMonth()) + 1;
        let yearNow = parseInt(new Date().getFullYear());
        let age = -1;

        if(parseInt(temp[1]) > monthNow) age = yearNow - parseInt(temp[2]) - 1;
        else if(parseInt(temp[1]) < monthNow) age = yearNow - parseInt(temp[2]);
        else {
            if(parseInt(temp[0]) > dateNow) age = yearNow - parseInt(temp[2]) - 1;
            else age = yearNow - parseInt(temp[2]);
        }
        return age+" Tahun";
    }

    function countLamaKerja(item){
        let temp = item.split(" ");
        let tglSkrg = (parseInt(new Date().getFullYear())*360) + ((parseInt(new Date().getMonth())+1)*30) + parseInt(new Date().getDate());
        let tglAwal = parseInt(temp[0]) + parseInt(temp[1]*30) + parseInt(temp[2]*360);
        let pengurangan = tglSkrg - tglAwal;

        //HITUNG TAHUN
        let tahun = Math.round(pengurangan/360);
        pengurangan -= (360*tahun);

        //HITUNG BULAN
        let bulan = Math.round(pengurangan/30);
        pengurangan -= (bulan*30);

        //HITUNG TANGGAL
        let tanggal = pengurangan;

        return tahun + " Tahun " + bulan + " Bulan " + tanggal + " Hari";
    }

    if(dataDetailEmployee.basic_salary){
        return(
            <>
                <Modal isOpen={isModal} onRequestClose={handleClosePopup} style={{overlay:{backgroundColor:'gray'},content:{color:'black', marginLeft:"auto", marginRight:"auto"}}}>
                    <center><img src={"data:image/png;base64, "+dataDetailEmployee.split1+dataDetailEmployee.split2} style={{width:"200px", height:"200px"}}/></center><br/>
                    <div className='row' style={{width:"100%",fontFamily:"arial"}}>
                        <div className="col-md-2">
                            Full Name         <br/>
                            Address           <br/>
                            Birth Date        <br/>
                            Age               <br/>
                            Gender            <br/>
                            Phone             <br/>
                            Jurusan Sekolah   <br/>
                            Nama Sekolah      <br/>
                            Department        <br/>
                            Position          <br/>
                        </div>
                        <div className="col-md-4" style={{borderRight:"1px solid black"}}>
                            : {dataDetailEmployee.name}                       <br/>
                            : {dataDetailEmployee.address}                    <br/>
                            : {dataDetailEmployee.birth_date}                 <br/>
                            : {countAge(dataDetailEmployee.birth_date_age)}   <br/>
                            : {dataDetailEmployee.gender}                     <br/>
                            : {dataDetailEmployee.phone}                      <br/>
                            : {dataDetailEmployee.jurusan_sekolah}            <br/>
                            : {dataDetailEmployee.nama_sekolah}               <br/>
                            : {dataDetailEmployee.department}                 <br/>
                            : {dataDetailEmployee.position}                   <br/>
                        </div>
                        <div className="col-md-2">
                            NIK                   <br/>
                            No Karyawan           <br/>
                            NPWP                  <br/>
                            No BCA                <br/>
                            No Kesehatan          <br/>
                            No Ketenagakerjaan    <br/>
                            Tanggal Kerja         <br/>
                            Lama Kerja            <br/>
                            Status Karyawan       <br/>
                        </div>
                        <div className="col-md-4">
                            : {dataDetailEmployee.nik}                        <br/>
                            : {dataDetailEmployee.nokaryawan}                 <br/>
                            : {dataDetailEmployee.npwp}                       <br/>
                            : {dataDetailEmployee.nobca}                      <br/>
                            : {dataDetailEmployee.nokesehatan}                <br/>
                            : {dataDetailEmployee.noketenagakerjaan}          <br/>
                            : {dataDetailEmployee.tglkerja}                   <br/>
                            : {countLamaKerja(dataDetailEmployee.lamakerja)}  <br/>
                            : {dataDetailEmployee.status_karyawan}            <br/>
                        </div>
                    </div><br/>

                    <center><h3>Total Salary : {"Rp. " + dataDetailEmployee.basic_salary.toString().replace(/\B(?=(\d{3})+(?!\d))/g, '.') + ",-"}</h3></center>
                    {/* ADDITIONAL COST (+) and NAME */}
                    
                </Modal>
            </>
        );
    }
    else return(<></>)
}
export default DetailEmployee;