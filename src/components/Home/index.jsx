import React, { useState, useEffect } from 'react';
import NavBar from "../NavBar/";
import SlipGaji from "./slipGaji"

const Home = () => {
    const token = window.sessionStorage.getItem("token");
    // const history= useHistory();
    const [ dataUser, setDataUser ] = useState([]);
    const [ foto, setFoto ] = useState([]);

    //FOR SLIPGAJI POPUP
    const [isModal, setIsModal] = useState(false);

    async function getDataUser(){
        try {
            const res = await fetch(`/api/getDataUser?token=${token}`, { 
                method : "GET",
                headers : { "Content-Type" : "application/json" }
            });
            let a = await res.json();

            setDataUser(a[0].hasil);
            setFoto(a[0].gambar);
            absentIn();
        } catch (error) {
            console.error(error);
        }
    }

    async function absentIn(){
        let obj= {
            token: token
        };
        try {
            const res = await fetch(`/absen/absenIn`, { 
                method : "POST",
                headers : { "Content-Type" : "application/json" },
                body: JSON.stringify(obj)
            });
            let a = await res.json();
            if(a[0].pesan) alert(a[0].pesan);
        } catch (error) {
            console.error(error);
        }
    }
    useEffect(() => {
        getDataUser();
    }, []);

    function handleShowPopupDetail(){
        setIsModal(true);
    }

    function handleCloseDetail(){
        setIsModal(false);
    }

    function countAge(item){
        let temp = item.split(" ");
        let dateNow = parseInt(new Date().getDate());
        let monthNow = parseInt(new Date().getMonth()) + 1;
        let yearNow = parseInt(new Date().getFullYear());
        let age = -1;

        if(parseInt(temp[1]) > monthNow) age = yearNow - parseInt(temp[2]) - 1;
        else if(parseInt(temp[1]) < monthNow) age = yearNow - parseInt(temp[2]);
        else {
            if(parseInt(temp[0]) > dateNow) age = yearNow - parseInt(temp[2]) - 1;
            else age = yearNow - parseInt(temp[2]);
        }
        return age+" Tahun";
    }

    function countLamaKerja(item){
        let temp = item.split(" ");
        let tglSkrg = (parseInt(new Date().getFullYear())*360) + ((parseInt(new Date().getMonth())+1)*30) + parseInt(new Date().getDate());
        let tglAwal = parseInt(temp[0]) + parseInt(temp[1]*30) + parseInt(temp[2]*360);
        let pengurangan = tglSkrg - tglAwal;

        //HITUNG TAHUN
        let tahun = Math.round(pengurangan/360);
        pengurangan -= (360*tahun);

        //HITUNG BULAN
        let bulan = Math.round(pengurangan/30);
        pengurangan -= (bulan*30);

        //HITUNG TANGGAL
        let tanggal = pengurangan;

        return tahun + " Tahun " + bulan + " Bulan " + tanggal + " Hari";
    }

    function print() {
        window.print()
    }

    return ( 
        <>
            <br/>
                <button onClick={print} style={{right:0,position:"absolute",marginRight:"15%"}}>Print<img src="https://img.icons8.com/carbon-copy/30/000000/print.png"/></button>
            <br/><br/>
            <center><img src={"data:image/png;base64, "+foto.split1+foto.split2} style={{width:"200px", height:"200px"}}/></center><br/>
            <div className='row' style={{width:"100%",fontFamily:"arial"}}>
                <NavBar/>
                <div className="col-md-3" style={{textAlign:"right"}}>
                    Name            :  <br/>
                    Address         :  <br/>
                    Birth Date      :  <br/>
                    Age             :  <br/>
                    Gender          :  <br/>
                    Phone           :  <br/>
                    Jurusan Sekolah :  <br/>
                    Nama Sekolah    :  <br/>
                    Department      :  <br/>
                    Position        :  <br/>
                </div>
                <div className="col-md-3" style={{borderRight:"1px solid black"}}>
                    {dataUser.name}                                                     <br/>
                    {dataUser.address}                                                  <br/>
                    {dataUser.birth_date}                                               <br/>
                    {dataUser.birth_date_age ? countAge(dataUser.birth_date_age) : ""}  <br/>
                    {dataUser.gender}                                                   <br/>
                    {dataUser.phone}                                                    <br/>
                    {dataUser.jurusan_sekolah}                                          <br/>
                    {dataUser.nama_sekolah}                                             <br/>
                    {dataUser.department}                                               <br/>
                    {dataUser.position}                                                 <br/>
                </div>
                <div className="col-md-3" style={{textAlign:"right"}}>
                    NIK                 :  <br/>
                    No Karyawan         :  <br/>
                    NPWP                :  <br/>
                    No BCA              :  <br/>
                    No Kesehatan        :  <br/>
                    No Ketenagakerjaan  :  <br/>
                    Tanggal Kerja       :  <br/>
                    Lama Kerja          :  <br/>
                    Status Karyawan     :  <br/>
                </div>
                <div className="col-md-3">
                    {dataUser.nik}                                                  <br/>
                    {dataUser.nokaryawan}                                           <br/>
                    {dataUser.npwp}                                                 <br/>
                    {dataUser.nobca}                                                <br/>
                    {dataUser.nokesehatan}                                          <br/>
                    {dataUser.noketenagakerjaan}                                    <br/>
                    {dataUser.tglkerja}                                             <br/>
                    {dataUser.lamakerja ? countLamaKerja(dataUser.lamakerja) : ""}  <br/>
                    {dataUser.status_karyawan}                                      <br/>
                </div>
            </div><br/><br/>
            <center>
                <button onClick={handleShowPopupDetail}>Slip Gaji</button>
            </center>

            <SlipGaji onClose= {handleCloseDetail} dataEmployee={dataUser} isModal={isModal}/>
        </>
     );
}
 
export default Home;