import React, { useState, useEffect } from 'react';
import Modal from 'react-modal';

const SlipGaji = ({onClose, dataEmployee, isModal}) => {
    function handleClosePopup(e){
        e.preventDefault();
        onClose();
    }
    return(
        <>  
            <Modal isOpen={isModal} onRequestClose={handleClosePopup} style={{overlay:{backgroundColor:'gray'},content:{color:'black', width:'80%', height:'70%', marginLeft:"auto", marginRight:"auto"}}}>
                <center><h1>Slip Gaji</h1></center><br/>
                <div className='row' style={{width:"100%",fontFamily:"arial"}}>
                    <div className="col-md-2">
                        Full Name         <br/>
                        Address           <br/>
                        Birth Date        <br/>
                        Gender            <br/>
                        Phone             <br/>
                        Jurusan Sekolah   <br/>
                        Nama Sekolah      <br/>
                        Department        <br/>
                        Position          <br/>
                    </div>
                    <div className="col-md-4" style={{borderRight:"1px solid black"}}>
                        : {dataEmployee.name}                       <br/>
                        : {dataEmployee.address}                    <br/>
                        : {dataEmployee.birth_date}                 <br/>
                        : {dataEmployee.gender}                     <br/>
                        : {dataEmployee.phone}                      <br/>
                        : {dataEmployee.jurusan_sekolah}            <br/>
                        : {dataEmployee.nama_sekolah}               <br/>
                        : {dataEmployee.department}                 <br/>
                        : {dataEmployee.position}                   <br/>
                    </div>
                    <div className="col-md-2">
                        NIK                   <br/>
                        No Karyawan           <br/>
                        NPWP                  <br/>
                        No BCA                <br/>
                        No Kesehatan          <br/>
                        No Ketenagakerjaan    <br/>
                        Tanggal Kerja         <br/>
                        Status Karyawan       <br/>
                    </div>
                    <div className="col-md-4">
                        : {dataEmployee.nik}                        <br/>
                        : {dataEmployee.nokaryawan}                 <br/>
                        : {dataEmployee.npwp}                       <br/>
                        : {dataEmployee.nobca}                      <br/>
                        : {dataEmployee.nokesehatan}                <br/>
                        : {dataEmployee.noketenagakerjaan}          <br/>
                        : {dataEmployee.tglkerja}                   <br/>
                        : {dataEmployee.status_karyawan}            <br/>
                    </div>
                </div><br/>

                {dataEmployee.basic_salary ? <center><h3>Total Salary : {"Rp. " + dataEmployee.basic_salary.toString().replace(/\B(?=(\d{3})+(?!\d))/g, '.') + ",-"}</h3></center> : ""}
                {/* ADDITIONAL COST (+) and NAME */}
            </Modal>
        </>
    )
}
export default SlipGaji;