import React, { useState } from 'react';
import DetailPenggajian from "./detailPenggajian"
import NavBar from "../NavBar/";
import ReactHTMLTableToExcel from 'react-html-table-to-excel';

const ManagementPenggajian = () => {
    const [excel, setExcel] = useState();

    async function processRegisterExcel(e){
        e.preventDefault();
        const formData= new FormData();
        formData.append("excel", excel);
        try {
            const res = await fetch("/m_penggajian/multiUploadPenggajian", { 
                method : "POST",
                body : formData
            });
            let a = await res.json();
            alert(a[0].pesan);
        } catch (error) {
            console.error(error);
        }
    }
    return(
        <>
            <NavBar/><br/><br/>
            <form onSubmit={processRegisterExcel} style={{position:"absolute",left:0,marginLeft:"33%",marginTop:"5%"}}>
                <center>
                    <label>Insert Employee(excel)                :  </label><input type="file" name="excel" onChange={ e => setExcel(e.target.files[0]) }/>
                    
                    <input type="submit" value="Submit"/> <br/><br/>

                    <ReactHTMLTableToExcel
                        id="test-table-xls-button"
                        className="download-table-xls-button"
                        table="template_penggajian"
                        filename="template_penggajian"
                        sheet="Sheet1"
                        buttonText="Download Template"/>
                        <table id="template_penggajian" hidden>
                            <tr>
                                <th>Name</th>
                                <th>No Karyawan</th>
                                <th>Status</th>
                                <th>Gaji</th>
                                <th>Department</th>
                                <th>Bulan</th>
                                <th>Tahun</th>
                                
                            </tr>
                            <tr>
                                <td>hanya boleh huruf, maksimal 255 karakter</td>
                                <td>maksimal 255 karakter</td>
                                <td>Pilihan : PKWT,PKWTT,OS</td>
                                <td>hanya boleh angka</td>
                                <td>Pilihan : Compound, Mandrel, Extrude, TPR PROSES, TPR COMPOUND, Teknik, Gudang, QC, HRGA, TUKANG, PPIC, R&D, S&M, EXIM, F&A, Purchasing, IT, Management, admin</td>
                                <td>hanya boleh angka (1-12)</td>
                                <td>hanya boleh angka</td>
                            </tr>
                        </table>
                </center>
            </form><br/>

            
                


            <div className='row' style={{width:"100%",fontFamily:"arial"}}>
                <div className="col-md-5">

                </div>
            </div>
        </>
    )
}
export default ManagementPenggajian;