import React, { useState, useEffect } from 'react';
import Modal from 'react-modal';

const DetailPenggajian = ({onClose, dataDetailSalary, dataHeaderSalary, isModal}) => {
    
    function handleClosePopup(e){
        e.preventDefault();
        onClose();
    }
    if(dataDetailSalary.length > 0){
        return(
            <>  
                <Modal isOpen={isModal} onRequestClose={handleClosePopup} style={{overlay:{backgroundColor:'gray'},content:{color:'black', width:'50%', height:'50%', marginLeft:"auto", marginRight:"auto"}}}>
                    <center><h1>Detail Salary</h1></center>
                    
                    {/* ADDITIONAL COST (+) and NAME */}
                    <div className='row' style={{width:"100%",fontFamily:"arial"}}>
                        <div className="col-md-5">
                            Full Name :          <br/><br/>
                            Basic Salary :       <br/>
                            Additional Cost(+) : <br/>
                            <ul>
                                <li>Transport Allowance              </li>
                                <li>Medical Allowance   &nbsp;&nbsp; </li>
                                <li>Position Allowance  &nbsp;&nbsp; </li>
                            </ul>
                        </div>
                        <div className="col-md-3">
                            <br/><br/><br/><br/>
                            {"Rp. " + dataDetailSalary[0].transport_allowance.toString().replace(/\B(?=(\d{3})+(?!\d))/g, '.') + ",-"}<br/>
                            {"Rp. " + dataDetailSalary[0].medical_allowance.toString().replace(/\B(?=(\d{3})+(?!\d))/g, '.') + ",-"}<br/>
                            {"Rp. " + dataDetailSalary[0].position_allowance.toString().replace(/\B(?=(\d{3})+(?!\d))/g, '.') + ",-"}<br/>
                        </div>
                        <div className="col-md-4">
                            {dataDetailSalary[0].name} <br/><br/>
                            {"Rp. " + dataDetailSalary[0].basic_salary.toString().replace(/\B(?=(\d{3})+(?!\d))/g, '.') + ",-"} <br/>
                            {"Rp. " + (dataDetailSalary[0].transport_allowance + dataDetailSalary[0].medical_allowance + dataDetailSalary[0].position_allowance).toString().replace(/\B(?=(\d{3})+(?!\d))/g, '.') + ",-"} <br/>

                        </div>
                    </div>

                    {/* ADDITIONAL COST (-) */}
                    <div className='row' style={{width:"100%",fontFamily:"arial"}}>
                        <div className="col-md-5">
                            Another Cost(+) : <br/>
                            <ul>
                                {
                                    dataDetailSalary.map((item, index) => {
                                        if(dataDetailSalary.length > 1) return (
                                            <li>{item.masalah}</li>
                                        )
                                    })
                                }
                            </ul><br/>
                        </div>
                        <div className="col-md-3">
                            <br/>
                            {
                                dataDetailSalary.map((item, index) => {
                                    if(dataDetailSalary.length > 1) return (
                                        <>{"Rp. " + item.nominal.toString().replace(/\B(?=(\d{3})+(?!\d))/g, '.') + ",-"}<br/></>
                                    )
                                })
                            }
                        </div>
                        <div className="col-md-4">
                            {"Rp. " + dataHeaderSalary.additional_min.toString().replace(/\B(?=(\d{3})+(?!\d))/g, '.') + ",-"} <br/>
                        </div>
                    </div>

                    <h3>Total Salary : {"Rp. " + dataHeaderSalary.salary.toString().replace(/\B(?=(\d{3})+(?!\d))/g, '.') + ",-"}</h3>
                </Modal>
            </>
        )
    }
    else return(<></>)
}
export default DetailPenggajian;