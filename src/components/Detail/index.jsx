import React, { useState, useEffect } from 'react';

const Detail = ({ match }) => {
    const [data, setData] = useState([]);

    async function getPage(){
        try {
            const res = await fetch("/api/getPhotoID?idPhoto=" + match.params.id, { 
                method : "GET",
                headers : { "Content-Type" : "application/json" }
            });
            let a = await res.json();
            setData(a[0].hasil);
            console.log(a[0].hasil);
        } catch (error) {
            console.error(error);
        }
    }

    useEffect(() => {
        getPage();
    }, []);

    return(
        <>
            <h1>{data.judul}</h1>
            <div className="card">
                <img className="card-img-top" style={{width:"400px",height:"250px"}} src={data.gambar} alt="Card image cap"></img>
            </div>
            <b>Deskripsi : </b><br/>
            <p>{data.deskripsi}</p>
        </>
    )
}
export default Detail;