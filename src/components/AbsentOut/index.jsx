import React, { useState, useEffect } from 'react';
import { useHistory } from 'react-router-dom';
import NavBar from "../NavBar/";
const AbsentOut = () => {
    const history= useHistory();
    const token = window.sessionStorage.getItem("token");
    const [ dataUser, setDataUser ] = useState([]);

    const [ attachment, setAttachment ] = useState("");

    async function getAbsensiUser(){
        try {
            const res = await fetch(`/absen/getAbsensiUser?token=${token}`, { 
                method : "GET",
                headers : { "Content-Type" : "application/json" }
            });
            let a = await res.json();
            setDataUser(a[0].hasil);
        } catch (error) {
            console.error(error);
        }
    }

    useEffect(() => {
        getAbsensiUser();
    }, []);

    async function processAbsentOut(e){
        e.preventDefault();
        if(window.confirm('Are You Sure Want to Absent Out?')){
            var hournow = new Date();
            if(attachment == "") {
                //alert("tidak ada attachment");
                if(hournow.getHours() < 17) alert("Attachment Mohon Diisi (Karena Absen Keluar Lebih Awal)");
                else {
                    try {
                        let obj= {
                            token: token
                        };
                        const res = await fetch("/absen/absenOut", { 
                            method : "POST",
                            headers : { "Content-Type" : "application/json" },
                            body : JSON.stringify(obj)
                        });
                        let a = await res.json();
                        alert(a[0].pesan);
                        history.push("/home");
                    } catch (error) {
                        console.error(error);
                    }
                }
            }
            else {
                //alert("ada attachment");
                //====================================== CODING INSERT PICTURE DISINI ==============================================================================
                
                try {
                    const formData= new FormData();
                    formData.append('token', token);
                    formData.append('image',attachment);
                    const res = await fetch("/absen/absenOutWithAttachment", { 
                        method : "POST",
                        body : formData
                    });
                    let a = await res.json();
                    alert(a[0].pesan);
                    if(a[0].pesan=="Absen Keluar telah tercatat"){
                        history.push("/home");
                    }
                } catch (error) {
                    console.error(error);
                }
            }
        }
    }

    async function logout(){
        let obj= {
            token: token
        };
        try {
            const res = await fetch(`/absen/logout`, { 
                method : "POST",
                headers : { "Content-Type" : "application/json" },
                body: JSON.stringify(obj)
            });
            let a = await res.json();
            window.sessionStorage.removeItem("token");
            history.push("/");
        } catch (error) {
            console.error(error);
        }
    }

    if(!dataUser){
        return (
            <>
                <NavBar/>
                <center>
                    <h1>Tidak Bisa Absen Sekarang :(</h1> 
                </center>
                
                {/* <button onClick={() => logout()}>Logout</button>
                <button onClick={() => history.push("/home")}>Home</button> */}
            </>
        );
    }
    else {
        if(dataUser.status_pulang_cepat != -1){
            return (
                <>
                    <NavBar/>
                    <center>
                        <h1>Sudah Absen :)</h1>    
                    </center>
                    {/* <button onClick={() => logout()}>Logout</button>
                    <button onClick={() => history.push("/home")}>Home</button>
                    { dataUser.department=="admin" ? <button onClick={() => history.push("/listkaryawan")}>Manage Employee</button> : ""} */}
                </>
            );
        }
        else {
            return ( 
                <>
                    <NavBar/>
                    <center><img src={"data:image/png;base64, "+dataUser.split1+dataUser.split2} style={{width:"200px", height:"200px"}}/></center><br/>
                    <div className='row' style={{width:"100%",fontFamily:"arial"}}>
                        <div className="col-md-4"></div>
                        <div className="col-md-2" style={{textAlign:"right"}}>
                            <label>Date         :  <br/></label>
                            <label>Name         :  <br/></label>
                            <label>Absent In    :  <br/></label>
                            <label>Attachment   :  <br/></label>
                        </div>
                        <div className="col-md-1" style={{textAlign:"left"}}>
                            <form onSubmit={processAbsentOut}>
                                <input type="date" value={dataUser.absent_date}                     readonly/>
                                <input type="text" value={dataUser.name}                            readonly/>
                                <input type="text" value={dataUser.absent_in}                       readonly/>
                                <input type="file" name="image" onChange={ e => setAttachment(e.target.files[0]) }/>
                                <input type="submit" value="Absent Out"/>
                            </form>
                        </div>
                        <div className="col-md-1"></div>
                        <div className="col-md-4"></div>
                    </div>
        
                    {/* <button onClick={() => logout()}>Logout</button>
                    <button onClick={() => history.push("/home")}>Home</button>
                    { dataUser.department=="admin" ? <button onClick={() => history.push("/listkaryawan")}>Manage Employee</button> : ""} */}
                </>
            );
        }
    }
    
}
 
export default AbsentOut;