import React, { useState, useEffect } from 'react';
import { useHistory } from 'react-router-dom';

const Recruitperson = () => {
    const history= useHistory();
    const token = window.sessionStorage.getItem("token");
    const [ dataUser, setDataUser ] = useState([]);
    const [ foto, setFoto ] = useState([]);

    //FIELD EDIT PROFILE
    const [image, setImage] = useState("");
    
    const [education, setEducation] = useState("SMA");
    const [department, setDepartment] = useState("IT");
    const [position, setPosition] = useState("MANAGER");
    const [status, setStatus] = useState("Single");
    const [selectGender, setSelectGender] = useState("");

    async function processEditProfile(e){
        e.preventDefault();
        const formData= new FormData();
        formData.append('name',e.target.firstname.value+" "+e.target.lastname.value);
        formData.append('address',e.target.address.value);
        formData.append('birthdate',e.target.birthdate.value);
        formData.append('city',e.target.city.value);
        formData.append('phone',e.target.phone.value);

        formData.append('education',education);
        formData.append('department',department);
        formData.append('position',position);
        formData.append('status',status);
        formData.append('gender',selectGender);

        formData.append('image',image);

        try {
            const res = await fetch("/m_recruitment/submitrecruitperson", { 
                method : "POST",
                body : formData
            });
            let a = await res.json();
            alert(a[0].pesan);
            if(a[0].pesan=="Recruit Person Submitted!"){
                history.push("/recruitment");
                window.location.reload();
            }
        } catch (error) {
            console.error(error);
        }
    }

    return ( 
        <>
            <center>
                <h1>Recruit Person :</h1>
                
            </center><br/>
            <div className='row' style={{width:"100%",fontFamily:"arial"}}>
                <div className="col-md-4"></div>
                <div className="col-md-2" style={{textAlign:"right"}}>
                    <label>First Name   :  </label><br/>
                    <label>Address      :  </label><br/>
                    <label>Birth Date   :  </label><br/>
                    <label>City         :  </label><br/>
                    <label>Phone        :  </label><br/>
                    <label>Education    :  </label><br/>
                    <label>Department   :  </label><br/>
                    <label>Position     :  </label><br/>
                    <label>Status       :  </label><br/>
                    <label>Photo        :  </label><br/>
                    <label>Gender       :  </label><br/>
                    
                </div>
                <div className="col-md-6" style={{textAlign:"left"}}>
                    <form onSubmit={processEditProfile}>
                        <input type="text" name="firstname" id="firstname"/> Last Name :  <input type="text" name="lastname" id="lastname"/> <br/>  
                        <input type="text" name="address" id="address"/> <br/>  
                        <input type="date" name="birthdate" id="birthdate"/> <br/> 
                        <input type="text" name="city" id="city"/> <br/>      
                        <input type="text" name="phone" id="phone"/> <br/>    
                        <select name="education" onChange={(e) => setEducation(e.target.value)}>
                            {status == "SMA" ? <option value="SMA" selected>SMA </option> : <option value="SMA">SMA</option>}
                            {status == "S1" ? <option value="S1"selected>S1</option> : <option value="S1">S1</option>}
                            {status == "S2" ? <option value="S2" selected>S2 </option> : <option value="S2">S2</option>}
                            {status == "S3" ? <option value="S3" selected>S3 </option> : <option value="S3">S3</option>}
                        </select> <br/>
                        <select name="department" onChange={(e) => setDepartment(e.target.value)}>
                            {status == "IT" ? <option value="IT" selected>IT </option> : <option value="IT">IT</option>}
                        </select> <br/>
                        <select name="Position" onChange={(e) => setPosition(e.target.value)}>
                            {status == "MANAGER" ? <option value="MANAGER" selected>MANAGER </option> : <option value="MANAGER">MANAGER</option>}
                            {status == "STAFF" ? <option value="STAFF" selected>STAFF </option> : <option value="STAFF">STAFF</option>}
                        </select> <br/>
                        <select name="status" onChange={(e) => setStatus(e.target.value)}>
                            {status == "Single" ? <option value="Single" selected>Single </option> : <option value="Single">Single</option>}
                            {status == "In a Relationship" ? <option value="In a Relationship"selected>In a Relationship</option> : <option value="In a Relationship">In a Relationship</option>}
                            {status == "Married" ? <option value="Married" selected>Married </option> : <option value="Married">Married</option>}
                            {status == "Widow / Widowed" ? <option value="Widow / Widowed" selected>Widow / Widowed </option> : <option value="Widow / Widowed">Widow / Widowed</option>}
                        </select> <br/>
                        <input type="file" name="image" id="image" onChange={ e => setImage(e.target.files[0]) }/> <br/>
                        <input type="radio" id="Male" name="gender" value="Male" checked={ selectGender==="Male" } onChange={ () => setSelectGender("Male") }/>
                                                        &nbsp;<img src="https://img.icons8.com/cotton/64/000000/male.png"/>
                                                        &emsp;
                                                        <input type="radio" id="Female" name="gender" value="Female" checked={ selectGender==="Female" } onChange={ () => setSelectGender("Female") }/>
                                                        &nbsp;<img src="https://img.icons8.com/cotton/64/000000/female.png"/>
                        <br/>
                        <input type="submit" value="Submit"/>
                    </form>
                </div>
            </div>
            <button onClick={() => history.push("/recruitment")}>Back</button>
        </>
     );
}
 
export default Recruitperson;