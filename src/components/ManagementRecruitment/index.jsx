import React, { useState, useEffect } from 'react';
import { useHistory } from 'react-router-dom';
import Badge from 'react-bootstrap/Badge';
import ListGroup from 'react-bootstrap/ListGroup';
import { Modal } from 'react-bootstrap';
import ModalHeader from 'react-bootstrap/esm/ModalHeader';
import NavBar from "../NavBar/";
const Managementrecruitment = () => {
    const history= useHistory();
    const token = window.sessionStorage.getItem("token")
    const [listrecruitment, setListrecruitment] = useState([]);
    const [detail, setDetail] = useState([]);
    const [status, setstatus] = useState([]);
    const [show, setShow] = useState(false);

    const [experience, setExperience] = useState("");
    const [project, setProject] = useState("");
    const [teamwork, setTeamwork] = useState("");
    const [hard_skill, setHard_skill] = useState("");
    const [soft_skill, setSoft_skill] = useState("");
    const [others, setOthers] = useState("");

    const [contract, setContract] = useState("");
    const [requirement, setRequirement] = useState("");
    const [salary, setSalary] = useState("");
    const [others_negotiation, setOthers_negotiation] = useState("");
    
    const [menu1, setMenu1] = useState("");
    const [menu2, setMenu2] = useState("");
    const [menu3, setMenu3] = useState("");

    const [checkbtnhasil, setCheckbtnhasil] = useState("");

    async function getrecruitment(){
        try {
            const res = await fetch(`/m_recruitment/getrecruitment`, { 
                method : "GET",
                headers : { "Content-Type" : "application/json" }
            });
            let a = await res.json();
            a.data= a.data.map(item => ({
                kode_recruitment: item.kode_recruitment,
                name: item.name,
                address: item.address,
                tanggal_lahir: item.tanggal_lahir,
                city: item.city,
                phone: item.phone,
                education: item.education,
                department: item.department,
                position: item.position,
                status: item.status,
                gender: item.gender,
                id_foto: item.id_foto,
                diterima: item.diterima,
                split1: item.split1,
                split2: item.split2,
                id_negotiation: item.id_negotiation,
                contract: item.contract,
                requirement: item.requirement,
                salary: item.salary,
                note_negotiation: item.note_negotiation,
                id_evaluation_skill: item.id_evaluation_skill,
                experience: item.experience,
                project: item.project,
                teamwork: item.teamwork,
                hard_skill: item.hard_skill,
                soft_skill: item.soft_skill,
                note_evaluation_skill: item.note_evaluation_skill,
            }));
            //console.log(a.data);
            setListrecruitment(a.data);
        } catch (error) {
            console.error(error);
        }
    }
    function pilihmenu1(){
        setMenu1("nav-link active");
        setMenu2("nav-link");
        setMenu3("nav-link");
        if(status=="Information"){
            setMenu3("nav-link disabled");
        }
    };
    function pilihmenu2(){
        setMenu1("nav-link");
        setMenu2("nav-link active");
        setMenu3("nav-link");
        if(status=="Information"){
            setMenu3("nav-link disabled");
        }
    };
    function pilihmenu3(){
        setMenu1("nav-link");
        setMenu2("nav-link");
        setMenu3("nav-link active");
    };

    function handleDetail(item,status_recruit){
        setShow(true);
        setDetail(item);
        setstatus(status_recruit);
        //evaluation skill
        setExperience(item.experience);
        setProject(item.project);
        setTeamwork(item.teamwork);
        setHard_skill(item.hard_skill);
        setSoft_skill(item.soft_skill);
        setOthers(item.note_evaluation_skill);
        //negotiation
        setContract(item.contract);
        setRequirement(item.requirement);
        setSalary(item.salary);
        setOthers_negotiation(item.note_negotiation);
        if(status_recruit=="Information"){
            setCheckbtnhasil(true);
            setMenu1("nav-link active");
            setMenu2("nav-link");
            setMenu3("nav-link");
            if(status_recruit=="Information"){
                setMenu3("nav-link disabled");
            }
        }
        else if(status_recruit=="Evaluation Skill"){
            setCheckbtnhasil(true);
            setMenu1("nav-link");
            setMenu2("nav-link active");
            setMenu3("nav-link");
            if(status_recruit=="Information"){
                setMenu3("nav-link disabled");
            }
        }
        else{
            setCheckbtnhasil(false);
            pilihmenu3();
        }
    }
    async function handlesubmit_evaluation_skill(e){
        e.preventDefault();
        try {
            let obj= {
                id_evaluation_skill: detail.id_evaluation_skill,
                experience: experience,
                project: project,
                teamwork: teamwork,
                hard_skill: hard_skill,
                soft_skill: soft_skill,
                note_evaluation_skill: others
            };
            const res = await fetch(`/m_recruitment/update_evaluation_skill`, { 
                method : "POST",
                headers : { "Content-Type" : "application/json" },
                body : JSON.stringify(obj)
            });
            let a = await res.json();
            alert(a.pesan);
            window.location.reload();
        } catch (error) {
            console.error(error);
        }
    }
    async function handlesubmit_negotiation(e){
        e.preventDefault();
        try {
            let obj= {
                id_negotiation: detail.id_negotiation,
                contract: contract,
                requirement: requirement,
                salary: salary,
                note_negotiation: others_negotiation
            };
            const res = await fetch(`/m_recruitment/update_negotiation`, { 
                method : "POST",
                headers : { "Content-Type" : "application/json" },
                body : JSON.stringify(obj)
            });
            let a = await res.json();
            alert(a.pesan);
            window.location.reload();
        } catch (error) {
            console.error(error);
        }
    }
    async function handlesubmit_hasil(e){
        e.preventDefault();
        try {
            let obj= {
                kode_recruitment: detail.kode_recruitment,
                hasil: e.target.hasil.value
            };
            const res = await fetch(`/m_recruitment/update_hasil`, { 
                method : "POST",
                headers : { "Content-Type" : "application/json" },
                body : JSON.stringify(obj)
            });
            let a = await res.json();
            alert(a.pesan);
            window.location.reload();
        } catch (error) {
            console.error(error);
        }
    }
    async function handlesearch(e){
        e.preventDefault();

        try {
            const res = await fetch(`/m_recruitment/getrecruitment/${e.target.search.value}`, { 
                method : "GET",
                headers : { "Content-Type" : "application/json" }
            });
            let a = await res.json();
            a.data= a.data.map(item => ({
                kode_recruitment: item.kode_recruitment,
                name: item.name,
                address: item.address,
                tanggal_lahir: item.tanggal_lahir,
                city: item.city,
                phone: item.phone,
                education: item.education,
                department: item.department,
                position: item.position,
                status: item.status,
                gender: item.gender,
                id_foto: item.id_foto,
                diterima: item.diterima,
                split1: item.split1,
                split2: item.split2,
                id_negotiation: item.id_negotiation,
                contract: item.contract,
                requirement: item.requirement,
                salary: item.salary,
                note_negotiation: item.note_negotiation,
                id_evaluation_skill: item.id_evaluation_skill,
                experience: item.experience,
                project: item.project,
                teamwork: item.teamwork,
                hard_skill: item.hard_skill,
                soft_skill: item.soft_skill,
                note_evaluation_skill: item.note_evaluation_skill,
            }));
            setListrecruitment(a.data);
        } catch (error) {
            console.error(error);
        }
    }
    function changepage(){
        if(menu1=="nav-link active"){
            return(
                <div className="mt-4">
                    <div className="card mb-4">
                        <div className="card-title" style={{ backgroundColor: 'rgb(1, 1, 50)' }}>
                            <h5 className="text-center text-white p-3">
                                Detail Information
                            </h5>
                        </div>
                        <div className="card-body" style={{ padding: '0% !important' }}>
                            <div className="row">
                                <div className="col-lg-12">
                                    <center>
                                        <img src={"data:image/png;base64, "+detail.split1+detail.split2} style={{width:"200px", height:"250px"}}/><br/>
                                    </center>
                                    <br/>
                                    Name : {detail.name} <br/>
                                    Address : {detail.address} <br/>
                                    BirthDate : {String(detail.tanggal_lahir).substr(0,10)} <br/>
                                    City : {detail.city} <br/>
                                    Phone : {detail.phone} <br/>
                                    Education : {detail.education} <br/>
                                    Department : {detail.department} <br/>
                                    Position : {detail.position} <br/>
                                    Status : {detail.status} <br/>
                                    Gender : {detail.gender} <br/>
                                </div>
                            </div>
                        </div>
                    </div> 
                </div> 
            );
        }
        else if(menu2=="nav-link active"){
            return(
                <div className="mt-4">
                    <div className="card mb-4">
                        <div className="card-title" style={{ backgroundColor: 'rgb(1, 1, 50)' }}>
                            <h5 className="text-center text-white p-3">
                                Detail Evaluation Skill
                            </h5>
                        </div>
                        <div className="card-body" style={{ padding: '0% !important' }}>
                            <div className="row">
                                <div className="col-lg-12">
                                    <form onSubmit={handlesubmit_evaluation_skill}>
                                        Experience : <br/><textarea name="" id="" cols="30" rows="2" value={experience} onChange={(e) => setExperience(e.target.value)}></textarea> <br/>
                                        Project : <br/> <textarea name="" id="" cols="30" rows="2" value={project} onChange={(e) => setProject(e.target.value)}></textarea> <br/>
                                        <br/>
                                        Teamwork : <select name="department" onChange={(e) => setTeamwork(e.target.value)}>
                                            {teamwork == "" ? <option value="" selected>Choose... </option> : <option value="">Choose...</option>}
                                            {teamwork == "YES" ? <option value="YES" selected>YES </option> : <option value="YES">YES</option>}
                                            {teamwork == "NO" ? <option value="NO" selected>NO </option> : <option value="NO">NO</option>}
                                        </select> <br/> <br/>
                                        
                                        Hard Skill : <br/> <textarea name="" id="" cols="30" rows="2" value={hard_skill} onChange={(e) => setHard_skill(e.target.value)}></textarea> <br/>
                                        Soft Skill : <br/> <textarea name="" id="" cols="30" rows="2" value={soft_skill} onChange={(e) => setSoft_skill(e.target.value)}></textarea> <br/>
                                        Others : <br/> <textarea name="" id="" cols="30" rows="2" value={others} onChange={(e) => setOthers(e.target.value)}></textarea> <br/>
                                        
                                        <input type="submit" value="Save"/>     
                                    </form>  
                                </div>
                            </div>
                        </div>
                    </div> 
                </div> 
            );
        }
        else if(menu3=="nav-link active"){
            return(
                <div className="mt-4">
                    <div className="card mb-4">
                        <div className="card-title" style={{ backgroundColor: 'rgb(1, 1, 50)' }}>
                            <h5 className="text-center text-white p-3">
                                Detail Negotiation
                            </h5>
                        </div>
                        <div className="card-body" style={{ padding: '0% !important' }}>
                            <div className="row">
                                <div className="col-lg-12">
                                    <form onSubmit={handlesubmit_negotiation}>
                                        Contract : <br/><input type="text" value={contract} onChange={(e) => setContract(e.target.value)}></input> <br/>
                                        Requirement : <br/> <textarea name="" id="" cols="30" rows="2" value={requirement} onChange={(e) => setRequirement(e.target.value)}></textarea> <br/>
                                        Salary : <br/> <input type="number" value={salary} onChange={(e) => setSalary(e.target.value)}></input> <br/>
                                        Others : <br/> <textarea name="" id="" cols="30" rows="2" value={others_negotiation} onChange={(e) => setOthers_negotiation(e.target.value)}></textarea> <br/>
                                        
                                        <input type="submit" value="Save"/>     
                                    </form> 
                                    <br/>
                                    <hr/>
                                    <center>
                                        <form onSubmit={handlesubmit_hasil}><input type="hidden" name="hasil" value="Accepted"/><input type="submit" value="Accept" disabled={checkbtnhasil}/></form><br/>
                                        <form onSubmit={handlesubmit_hasil}><input type="hidden" name="hasil" value="Rejected"/><input type="submit" value="Declined" disabled={checkbtnhasil}/></form>
                                    </center>
                                </div>
                            </div>
                        </div>
                    </div> 
                </div> 
            );
        }
    }
    function handleClose() { setShow(false); }
    useEffect(() => {
        getrecruitment();
    }, []);

    return ( 
        <>
            <NavBar/>

            <button onClick={() => history.push("/recruitperson")} style={{position:"absolute",right:0,marginRight:"2%",marginTop:"1%"}}>Recruit Person</button>
            <center>
                <h1>Management Recruitment</h1>
            </center><br/>
            <div className='row' style={{width:"100%",fontFamily:"arial"}}>
                <div className="col-md-2"></div>
                <div className="col-md-8">
                    <center>
                        <form onSubmit={handlesearch}>
                            Name : <input type="text" name="search" id="search"/>  <input type="submit" value="search"/>
                        </form>
                    </center>
                    <br/><br/>  
                    <ListGroup>
                        {
                            listrecruitment.map((item, index) => {
                                let status_recruit="";
                                let cek=0;
                                let color="black"
                                if(item.experience=="" || item.project=="" ||item.teamwork=="" ||item.hard_skill=="" ||item.soft_skill=="" )cek=1;//information
                                if(cek==0){if(item.contract=="" || item.requirement=="")cek=2;}//evaluation skill
                                if(cek==0){if(item.diterima=="")cek=3;}//negotiation
                                //hasil
                                if(cek==0){
                                    status_recruit=item.diterima;
                                    if(String(item.diterima).toUpperCase()=="ACCEPTED")color="green";
                                    else if(String(item.diterima).toUpperCase()=="REJECTED")color="red";
                                }
                                if(cek==1)status_recruit="Information";
                                if(cek==2)status_recruit="Evaluation Skill";
                                if(cek==3)status_recruit="Negotiation";
                                return (
                                    <ListGroup.Item>
                                        <b>{item.name}</b> <br/>
                                        {item.department} - <label style={{color:color}}>{status_recruit}</label>
                                        <button style={{float:"right"}}  onClick={ () => handleDetail(item,status_recruit) }>Detail</button>
                                    </ListGroup.Item>
                                )
                            })
                        }
                    </ListGroup>
                </div>
                <div className="col-md-2"></div>
            </div>
            {/* <button onClick={() => history.push("/home")}>Back</button> */}
            <Modal size="lg" style={{ fontSize: '1.25vw' }}
                   show={ show } 
                   onHide={ handleClose }>
                    { detail ? 
                        <>
                            <Modal.Header closeButton>
                                <Modal.Title>
                                    Detail Recruitment
                                </Modal.Title>
                            </Modal.Header>
                            <Modal.Body>
                                <ul className="nav nav-tabs">
                                    <li className="nav-item">
                                        <a className={menu1} onClick={pilihmenu1}>Information</a>
                                    </li>
                                    <li className="nav-item">
                                        <a className={menu2} onClick={pilihmenu2}>Evaluation Skill</a>
                                    </li>
                                    <li className="nav-item">
                                        <a className={menu3} onClick={pilihmenu3}>Negotiation</a>
                                    </li>
                                </ul>
                                { changepage() }
                            </Modal.Body>
                        </>
                    : '' }
            </Modal>
        </>
     );
}
 
export default Managementrecruitment;