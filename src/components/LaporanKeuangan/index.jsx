import React, { useState, useEffect } from 'react';
import { useHistory } from 'react-router-dom';
import { MDBDataTableV5 } from 'mdbreact';
import ChartBar from './chartBar';
import ChartPie from './chartPie';
import FormFilter from './FormFilter'
import DetailKeuangan from './DetailKeuangan'
import DetailSalary from './DetailSalary'
import NavBar from "../NavBar/";

const LaporanKeuangan = () => {
    const history= useHistory();
    const [ dataSalary, setDataSalary ] = useState([]);
    const [ dataKeuangan, setDataKeuangan ] = useState([]);
    const [ totalSalary, setTotalSalary] = useState("");
    const [ totalKeuangan, setTotalKeuangan] = useState("");

    //FOR TABLE
    const [ dataTable, setDataTable ]= useState();
    const [ dataSetTable, setDataSetTable] = useState();
    const [ dataTable2, setDataTable2 ]= useState();
    const [ dataSetTable2, setDataSetTable2] = useState();

    //FOR CHART
    const [ chartData, setChartData ] = useState({});

    //FOR DETAIL KEUANGAN
    const [ keuangan, setKeuangan ] = useState([]);
    const [ isModalKeuangan, setIsModalKeuangan ] = useState(false);

    //FOR DETAIL SALARY
    const [ salary, setSalary ] = useState([]);
    const [ isModalSalary, setIsModalSalary ] = useState(false);
    const [ totalDetailSalary, setTotalDetailSalary] = useState();

    async function getDataLaporan(){
        try {
            const res = await fetch(`/laporan/getDataLaporan`, { 
                method : "GET",
                headers : { "Content-Type" : "application/json" }
            });
            let a = await res.json();
            setDataSalary(a[0].salary);
            setDataKeuangan(a[0].keuangan);
            setTotalSalary(a[0].total_salary)

            setAllTable(a[0].salary, a[0].keuangan);
        } catch (error) {
            console.error(error);
        }
    }
    function print() {
        window.print()
    }
    function setAllTable(salary, keuangan){
         //SET TABLE FOR SALARY
        let tempTable = salary.map((item, index) => {
            return ({
                row: index+1,
                type: `Salary`,
                month: item.month,
                year: item.year,
                total: "Rp. " + item.total.toString().replace(/\B(?=(\d{3})+(?!\d))/g, '.') + ",-",
                status: item.status_karyawan,

                detail: <button onClick={() => handlePopupDetailSalary(item) } 
                        style={{fontSize:"0.7vw", textAlign:"center"}}
                        className="btn btn-primary">
                        &nbsp; Detail &nbsp;
                        </button>
                    
            })
        });
        setDataSetTable(tempTable);

        setDataTable({
            columns:  [
                { label: '#', field: 'row' },
                { label: 'TYPE', field: 'type' },
                { label: 'MONTH', field: 'month' },
                { label: 'PERIODE', field: 'year' },
                { label: 'TOTAL', field: 'total' },
                { label: 'STATUS', field: 'status' },
                { label: 'DETAIL', field: 'detail' }
            ],
            rows: tempTable
        });


        //SET TABLE FOR KEUANGAN
        tempTable = keuangan.map((item, index) => {
            return ({
                row: index+1,
                name: item.name,
                month: item.month,
                year: item.year,
                total: "Rp. " + item.total.toString().replace(/\B(?=(\d{3})+(?!\d))/g, '.') + ",-",
                status: item.status,
                department: item.department,

                detail: <button onClick={() => handlePopupDetailKeuangan(item) } 
                        style={{fontSize:"0.7vw", textAlign:"center"}}
                        className="btn btn-primary">
                        &nbsp; Detail &nbsp;
                        </button>
                    
            })
        });
        setDataSetTable2(tempTable);

        setDataTable2({
            columns:  [
                { label: '#', field: 'row' },
                { label: 'NAME', field: 'name' },
                { label: 'MONTH', field: 'month' },
                { label: 'PERIODE', field: 'year' },
                { label: 'SALARY', field: 'total' },
                { label: 'STATUS', field: 'status' },
                { label: 'DEPARTMENT', field: 'department' },
                { label: 'DETAIL', field: 'detail' }
            ],
            rows: tempTable
        });
    }

    useEffect(() => {
        getDataLaporan();
    }, []);


    function handlePopupDetailKeuangan(item){
        setKeuangan(item);
        setIsModalKeuangan(true);
    }
    async function handlePopupDetailSalary(item){
        try {
            const res = await fetch(`/laporan/getDetailSalary?month=${item.month}&year=${item.year}&status=${item.status_karyawan}`, { 
                method : "GET",
                headers : { "Content-Type" : "application/json" }
            });
            let a = await res.json();
            setSalary(a[0].hasil);
            setIsModalSalary(true);
            setTotalDetailSalary(item.total);
        } catch (error) {
            console.error(error);
        }
    }
    function closeDetail(){
        setIsModalKeuangan(false);
        setIsModalSalary(false);
    }

    async function getDataChart(obj){
        try {
            const data = await fetch("/laporan/getDataChart?month=" + obj.month + "&year=" + obj.year, { 
                method: "GET",
                headers : { "Content-Type" : "application/json" }
            });
            let temp = await data.json();
            setChart(temp);
          } catch (error) {
              console.error(error);
          }
    }
    function setChart(temp){
        setChartData({
            labels: temp[0].nama,
            datasets:[
              {
                label:'Chart Sold Shoes',
                data:temp[0].jumlah,
                backgroundColor:[
                  'rgba(85, 85, 85, 0.6)',
                  'rgba(237, 231, 52, 1)',
                  'rgba(233, 27, 32, 1)',
                  'rgba(1, 1, 50, 1)',
    
                  'rgba(85, 85, 85, 0.6)',
                  'rgba(237, 231, 52, 1)',
                  'rgba(233, 27, 32, 1)',
                  'rgba(1, 1, 50, 1)',
    
                  'rgba(85, 85, 85, 0.6)',
                  'rgba(237, 231, 52, 1)',
                  'rgba(233, 27, 32, 1)',
                  'rgba(1, 1, 50, 1)',
                ]
                
              }
            ]
        });
    }

    async function handleAddItem(obj) {
        try {
            const res = await fetch("/laporan/getLaporanFilter?month=" + obj.month + "&year=" + obj.year, { 
                method : "GET",
                headers : { "Content-Type" : "application/json" }
            });
            let a = await res.json();
            //console.log(a);
            setDataSalary(a[0].salary);
            setDataKeuangan(a[0].keuangan);
            setTotalSalary(a[0].total_salary)

            setAllTable(a[0].salary, a[0].keuangan);
        } catch (error) {
            console.error(error);
        }
    }

    return ( 
        <>
            <NavBar/>
            <br/>
                <button onClick={print} style={{right:0,position:"absolute",marginRight:"15%"}}>Print<img src="https://img.icons8.com/carbon-copy/30/000000/print.png"/></button>
            <br/><br/>
            <center><h1>Financial Report</h1></center><br/><br/>
            <center><FormFilter onAddItem={handleAddItem} /></center>
            <div className='row' style={{width:"100%",fontFamily:"arial"}}>
                <div className="col-md-6">
                    <h3>Salary Report by Month</h3>
                    { dataTable && dataTable.rows.length ?     
                    <MDBDataTableV5 responsive
                                    striped
                                    searchBottom={ false }
                                    data={ dataTable } />
                    : 'No Employee.' }
                </div>
                <div className="col-md-6">
                    <h3>Employee Report</h3>
                    { dataTable2 && dataTable2.rows.length ?     
                    <MDBDataTableV5 responsive
                                    striped
                                    searchBottom={ false }
                                    data={ dataTable2 } />
                    : 'No Employee.' }
                </div>
            </div>
            <div className='row' style={{width:"100%",fontFamily:"arial"}}>
                <div className="col-md-6">
                    <h4>Total : {totalSalary ? "Rp. " + totalSalary.toString().replace(/\B(?=(\d{3})+(?!\d))/g, '.') + ",-" : ""}</h4>
                    <br/><br/>
                </div>
                <div className="col-md-6">
                    <h4>Total : {totalSalary ? "Rp. " + totalSalary.toString().replace(/\B(?=(\d{3})+(?!\d))/g, '.') + ",-" : ""}</h4>
                    <br/><br/>
                    {/* <h2>Total Pengeluaran : {totalSalary ? "Rp. " + (totalSalary+totalKeuangan).toString().replace(/\B(?=(\d{3})+(?!\d))/g, '.') + ",-" : ""}</h2> */}
                </div>
            </div>
            <br/><br/><br/><br/><hr/>
            
            <center><FormFilter onAddItem={getDataChart} /></center><br/>
            <ChartBar chartData={chartData} location="Massachusetts" legendPosition="top"/>
            <ChartPie chartData={chartData} location="Massachusetts" legendPosition="top"/>

            <DetailKeuangan onClose= {closeDetail} keuangan={keuangan} isModal={isModalKeuangan} />
            <DetailSalary onClose= {closeDetail} salary={salary} totalDetailSalary={totalDetailSalary} isModal={isModalSalary} />

            {/* <button onClick={() => history.push("/home")}>Back</button> */}
        </>
     );
}
 
export default LaporanKeuangan;