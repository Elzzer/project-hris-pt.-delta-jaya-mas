import React, { useState } from 'react';
import useFormInput from "./../../libraries/useFormInput"

const FormFilter = ({ onAddItem }) => {
    const month = useFormInput("");
    const year = useFormInput("");

    const handleSubmit = (e) => {
        e.preventDefault();
        onAddItem({ month : month.value, year : year.value});
    }

    return ( 
        <form onSubmit={handleSubmit}>
            Month :         <input type="text" { ...month } /><br/><br/>
            Year  :&emsp;   <input type="text" { ...year } /><br/><br/>

            <button type="submit" style={{fontSize:"0.7vw", textAlign:"center"}} className="btn btn-primary">&nbsp; Submit &nbsp;</button>
            <br/><br/><br/>
        </form>
    );
}
 
export default FormFilter;