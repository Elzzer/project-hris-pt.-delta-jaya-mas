import React from 'react';
import Modal from 'react-modal';

const DetailKeuangan = ({onClose, keuangan, isModal}) => {
    function handleClosePopup(e){
        e.preventDefault();
        onClose();
    }

    

    return (
        <>  
            <Modal isOpen={isModal} onRequestClose={handleClosePopup} style={{overlay:{backgroundColor:'gray'},content:{marginRight:'auto', marginLeft:'auto', color:'black', width:'50%', height:'50%'}}}>
                <center><h1>Detail Salary</h1></center><br/>
                <div className='row' style={{width:"100%",fontFamily:"arial"}}>
                    <div className="col-md-4">
                        Nama                    <br/>
                        Nomor Induk Karyawan    <br/>
                        Status                  <br/>
                        Department              <br/><br/>
                        Bulan                   <br/>
                        Periode                 <br/>
                    </div>
                    <div className="col-md-1">
                        :<br/>
                        :<br/>
                        :<br/>
                        :<br/><br/>
                        :<br/>
                        :<br/>
                    </div>
                    <div className="col-md-7">
                        {keuangan.name}       <br/>
                        {keuangan.nokaryawan} <br/>
                        {keuangan.status}     <br/>
                        {keuangan.department} <br/><br/>
                        {keuangan.month}      <br/>
                        {keuangan.year}       <br/>
                    </div>
                </div><br/><br/>
                {keuangan.total ? <h4>Total Salary : {"Rp. " + keuangan.total.toString().replace(/\B(?=(\d{3})+(?!\d))/g, '.') + ",-"}</h4> : ""}
            </Modal>
        </>
    )
}
export default DetailKeuangan;