import React from 'react';
import Modal from 'react-modal';

const DetailKeuangan = ({onClose, salary, totalDetailSalary, isModal}) => {
    function handleClosePopup(e){
        e.preventDefault();
        onClose();
    }

    return (
        <>
            <Modal isOpen={isModal} onRequestClose={handleClosePopup} style={{overlay:{backgroundColor:'gray'},content:{marginRight:'auto', marginLeft:'auto', color:'black', width:'50%'}}}>
            <center><h1>Detail Salary</h1></center><br/>
            {
                salary.map((item, index) => {
                    return (
                        <>
                            <br/>
                            <div className='row' style={{width:"100%",fontFamily:"arial"}}>
                                <div className="col-md-4">
                                    Nama                    <br/>
                                    Nomor Induk Karyawan    <br/>
                                    Status                  <br/>
                                    Department              <br/><br/>
                                    Bulan                   <br/>
                                    Periode                 <br/>
                                </div>
                                <div className="col-md-1">
                                    :<br/>
                                    :<br/>
                                    :<br/>
                                    :<br/><br/>
                                    :<br/>
                                    :<br/>
                                </div>
                                <div className="col-md-7">
                                    {item.name}       <br/>
                                    {item.nokaryawan} <br/>
                                    {item.status}     <br/>
                                    {item.department} <br/><br/>
                                    {item.bulan}      <br/>
                                    {item.tahun}       <br/>
                                </div>
                            </div><br/><br/>
                            {item.gaji ? <h4>Total Salary : {"Rp. " + item.gaji.toString().replace(/\B(?=(\d{3})+(?!\d))/g, '.') + ",-"}</h4> : ""}
                            <br/>
                            <hr/>
                        </>
                    )
                })
            }
            {totalDetailSalary ? <h2>Total : {"Rp. " + totalDetailSalary.toString().replace(/\B(?=(\d{3})+(?!\d))/g, '.') + ",-"}</h2> : ""}
            </Modal>
        </>
    )
    // if(salary){
    //     return(
    //         <>  
    //             <Modal isOpen={isModal} onRequestClose={handleClosePopup} style={{overlay:{backgroundColor:'gray'},content:{color:'black'}}}>
    //                 <center><h1>Detail Salary</h1></center><br/><br/>
    //                 {
    //                     salary.map((item, index) => {
    //                         return (
    //                             <>
    //                                 <div className='row' style={{width:"100%",fontFamily:"arial"}}>
    //                                     <div className="col-md-5">
    //                                         Full Name :          <br/><br/>
    //                                         Basic Salary :       <br/>
    //                                         Additional Cost(+) : <br/>
    //                                         <ul>
    //                                             <li>Transport Allowance              </li>
    //                                             <li>Medical Allowance   &nbsp;&nbsp; </li>
    //                                             <li>Position Allowance  &nbsp;&nbsp; </li>
    //                                         </ul>
    //                                     </div>
    //                                     <div className="col-md-3">
    //                                         <br/><br/><br/><br/>
    //                                         {"Rp. " + item.transport_allowance.toString().replace(/\B(?=(\d{3})+(?!\d))/g, '.') + ",-"}<br/>
    //                                         {"Rp. " + item.medical_allowance.toString().replace(/\B(?=(\d{3})+(?!\d))/g, '.') + ",-"}<br/>
    //                                         {"Rp. " + item.position_allowance.toString().replace(/\B(?=(\d{3})+(?!\d))/g, '.') + ",-"}<br/>
    //                                     </div>
    //                                     <div className="col-md-4">
    //                                         {item.name} <br/><br/>
    //                                         {"Rp. " + item.basic_salary.toString().replace(/\B(?=(\d{3})+(?!\d))/g, '.') + ",-"} <br/>
    //                                         {"Rp. " + (item.transport_allowance + item.medical_allowance + item.position_allowance).toString().replace(/\B(?=(\d{3})+(?!\d))/g, '.') + ",-"} <br/>

    //                                     </div>
    //                                 </div>

    //                                 <div className='row' style={{width:"100%",fontFamily:"arial"}}>
    //                                     <div className="col-md-5">
    //                                         Another Cost(+) : <br/>
    //                                         <ul>
    //                                             {
    //                                                 semuaKeuangan.map((itemKeuangan, index) => {
    //                                                     let temp = item.id_keuangan.split(",");
    //                                                     for(let i=0; i<temp.length-1; i++){
    //                                                         if(itemKeuangan.id_keuangan == temp[i]) 
    //                                                         return (
    //                                                             <li>{itemKeuangan.masalah}</li>
    //                                                         )
    //                                                     }
    //                                                 })
    //                                             }
    //                                         </ul><br/>
    //                                     </div>
    //                                     <div className="col-md-3">
    //                                         <br/>
    //                                         {
    //                                             semuaKeuangan.map((itemKeuangan, index) => {
    //                                                 let temp = item.id_keuangan.split(",");
    //                                                 for(let i=0; i<temp.length-1; i++){
    //                                                     if(itemKeuangan.id_keuangan == temp[i]) 
    //                                                     return (
    //                                                         <>{"Rp. " + itemKeuangan.nominal.toString().replace(/\B(?=(\d{3})+(?!\d))/g, '.') + ",-"}<br/></>
    //                                                     )
    //                                                 }
    //                                             })
    //                                         }
    //                                     </div>
    //                                     <div className="col-md-4">
    //                                         {/* {"Rp. " + dataHeaderSalary.additional_min.toString().replace(/\B(?=(\d{3})+(?!\d))/g, '.') + ",-"} <br/> */}
    //                                     </div>
    //                                 </div>
    //                                 <h4>Total : {hitungTotalSalary(item)}</h4>
    //                                 <hr/>
    //                             </>
    //                         )
    //                     })
    //                 }
                    
    //                 <h2>Grand Total : {totalSalary ? "Rp. " + totalSalary.toString().replace(/\B(?=(\d{3})+(?!\d))/g, '.') + ",-" : ""}</h2>
    //             </Modal>
    //         </>
    //     )
    // }
}
export default DetailKeuangan;