import React, { useState } from 'react';
import { useHistory } from 'react-router-dom';

const Login = () => {
    const history= useHistory();

    async function processLogin(e){
        e.preventDefault();
        let obj= {
            phone: e.target.phone.value,
            password: e.target.password.value
        };
        try {
            const res = await fetch("/api/login", { 
                method : "POST",
                headers : { "Content-Type" : "application/json" },
                body : JSON.stringify(obj)
            });
            let a = await res.json();
            alert(a[0].pesan);
            if(a[0].pesan == "Berhasil Login") {
                window.sessionStorage.setItem("token",a[0].tokenUser);
                history.push("/home");
            }
        } catch (error) {
            console.error(error);
        }
    }

    return ( 
        <>
            <center><h1>LOGIN PAGE</h1></center><br/>
            <form onSubmit={processLogin}>
                <div className='row' style={{width:"100%",fontFamily:"arial"}}>
                    <div className="col-md-3"></div>

                    <div className="col-md-2" style={{textAlign:"right"}}>
                        <label>Phone :  </label><br/>
                        <label>Password :  </label><br/>
                    </div>
                    <div className="col-md-2" style={{marginLeft:"2%"}}>
                        <input type="text" name="phone" id="phone"/><br/>
                        <input type="password" name="password" id="password"/><br/><br/>
                        <input type="submit" value="Login"/> &emsp;&nbsp;
                        <button onClick={() => history.push("/register")}>Go To Register</button>
                    </div>
                    
                    <div className="col-md-2"></div>
                    <div className="col-md-3"></div>
                </div>
            </form>
        </>
     );
}
 
export default Login;