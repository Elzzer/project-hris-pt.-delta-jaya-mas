import React, { useState, useEffect } from 'react';
import { useHistory } from 'react-router-dom';
import './index.css';
import ReactHTMLTableToExcel from 'react-html-table-to-excel';

const Register = () => {
    const history= useHistory();
    const [ selectEducation, setSelectEducation ] = useState("SMA");
    const [ selectStatus, setSelectStatus ] = useState("Single");
    const [ selectGender, setSelectGender ] = useState("Male");
    const [ selectDepartment, setSelectDepartment ] = useState("Compound");
    const [ selectPosition, setSelectPosition ] = useState("Manager");
    const [image, setImage] = useState();
    const [excel, setExcel] = useState();
    const [datanomor, setDatanomor] = useState([]);
    const [check, setCheck] = useState("cross.png");
    const [checkconfirmpassword, setCheckconfirmpassword] = useState("cross.png");
    const [confirmpassword, setConfirmpassword] = useState("");
    const [password, setPassword] = useState("");


    function handlePassword(e){
        setPassword(e.target.value);
        if(e.target.value){
            if(e.target.value==confirmpassword){
                setCheckconfirmpassword("check.png");
            }
            else{
                setCheckconfirmpassword("cross.png");
            }
        }
    }
    function handleConfirmPassword(e){
        setConfirmpassword(e.target.value);
        if(e.target.value){
            if(e.target.value==password){
                setCheckconfirmpassword("check.png");
            }
            else{
                setCheckconfirmpassword("cross.png");
            }
        }
    }
    function handleNomor(e){
        if(e.target.value==""){
            setCheck("cross.png");
        }
        else{
            let berhasil=true;
            datanomor.map((item, index) => {
                if(item.phone==e.target.value){
                    berhasil=false;
                }
            });
            if(berhasil){
                setCheck("check.png");
            }
            else{
                setCheck("cross.png");
            }
        }
    }
    async function getuser(){
        try {
            const res = await fetch(`/api/getuser`, { 
                method : "GET",
                headers : { "Content-Type" : "application/json" }
            });
            let a = await res.json();
            a.user= a.user.map(item => ({
                phone : item.phone
            }));
            setDatanomor(a.user);
        } catch (error) {
            console.error(error);
        }
    }  
    useEffect(() => {
        getuser();
    }, []);

    async function processRegister(e){
        e.preventDefault();

        // if(e.target.firstname.value == "") e.target.firstname.value = "-";
        // if(e.target.lastname.value == "") e.target.lastname.value = "-";
        // if(e.target.address.value == "") e.target.address.value = "-";
        // if(e.target.birthdate.value == "") e.target.birthdate.value = "-";
        // if(e.target.city.value == "") e.target.city.value = "-";
        // if(e.target.phone.value == "") e.target.phone.value = "-";
        // if(e.target.password.value == "") e.target.password.value = "-";
        // if(e.target.confirmpassword.value == "") e.target.confirmpassword.value = "-";
        // if(e.target.jurusan_sekolah.value == "") e.target.jurusan_sekolah.value = "-";
        // if(e.target.nama_sekolah.value == "") e.target.nama_sekolah.value = "-";
        // if(e.target.nik.value == "") e.target.nik.value = "-";
        // if(e.target.nokaryawan.value == "") e.target.nokaryawan.value = "-";
        // if(e.target.npwp.value == "") e.target.npwp.value = "-";
        // if(e.target.nobca.value == "") e.target.nobca.value = "-";
        // if(e.target.nokesehatan.value == "") e.target.nokesehatan.value = "-";
        // if(e.target.noketenagakerjaan.value == "") e.target.noketenagakerjaan.value = "-";
        // if(e.target.tglkerja.value == "") e.target.tglkerja.value = "-";
        // if(e.target.status_karyawan.value == "") e.target.status_karyawan.value = "-";

        // if(e.target.basic_salary.value == "") e.target.basic_salary.value = 0;
        // if(e.target.transport_salary.value == "") e.target.transport_salary.value = 0;
        // if(e.target.medical_salary.value == "") e.target.medical_salary.value = 0;
        // if(e.target.position_salary.value == "") e.target.position_salary.value = 0;

        const formData= new FormData();
            formData.append('nama', e.target.nama.value);
            formData.append("address", e.target.address.value);
            formData.append("birthdate", e.target.birthdate.value);
            formData.append("city", e.target.city.value);
            formData.append("phone", e.target.phone.value);
            formData.append("password", e.target.password.value);
            formData.append("confirmpassword", e.target.confirmpassword.value);

            formData.append("jurusan_sekolah", e.target.jurusan_sekolah.value);
            formData.append("nama_sekolah", e.target.nama_sekolah.value);

            formData.append("education", selectEducation);
            formData.append("status", selectStatus);
            formData.append("gender", selectGender);
            formData.append("image", image);

            formData.append("nik", e.target.nik.value);
            formData.append("nokaryawan", e.target.nokaryawan.value);
            formData.append("npwp", e.target.npwp.value);
            formData.append("nobca", e.target.nobca.value);
            formData.append("nokesehatan", e.target.nokesehatan.value);
            formData.append("noketenagakerjaan", e.target.noketenagakerjaan.value);
            formData.append("tglkerja", e.target.tglkerja.value);
            formData.append("status_karyawan", e.target.status_karyawan.value);

            formData.append("department", selectDepartment);
            formData.append("position", selectPosition);

            formData.append("basic_salary", e.target.basic_salary.value);

            let berhasil=true;
            for (var value of formData.values()) {
                if(value=="" || value==null){
                    berhasil=false;
                } 
            }
            if(berhasil && check=="check.png"){
                try {
                    const res = await fetch("/api/register", { 
                        method : "POST",
                        body : formData
                    });
                    let a = await res.json();
                    alert(a[0].pesan);
                    if(a[0].pesan == "Success Register"){
                        history.push("/");
                        window.location.reload();
                    }
                } catch (error) {
                    console.error(error);
                }
            }
            else{
                alert("terdapat field yang kosong atau nomor yang kembar !");
            }
    }
    async function processRegisterExcel(e){
        e.preventDefault();
        const formData= new FormData();
        formData.append("excel", excel);
        try {
            const res = await fetch("/api/multiregister", { 
                method : "POST",
                body : formData
            });
            let a = await res.json();
            alert(a[0].pesan);
            if(a[0].pesan == "Success Register"){
                history.push("/");
                window.location.reload();
            }
        } catch (error) {
            console.error(error);
        }
    }

    return ( 
        <>
            <center><h1>REGISTER PAGE</h1></center><br/>
            <form onSubmit={processRegister}>
                <div className='row' style={{width:"100%",fontFamily:"arial"}}>
                    {/* ================================= FORM DATA DIRI ================================= */}
                    <div className="col-md-6" style={{paddingLeft:"30px", borderRight:"1px solid black"}}>
                        <h4><b>Data Employee</b></h4>
                        <label>Name           :  </label><input type="text" name="nama" id="nama"/> <br/>
                        <label>Address              :  </label><input type="text" name="address" id="address"/>                         <br/>
                        <label>Birth Date           :  </label><input type="date" name="birthdate" id="birthdate"/>                     <br/>
                        <label>City                 :  </label><input type="text" name="city" id="city"/>                               <br/>
                        <label>Phone                :  </label><input type="text" onChange={handleNomor} name="phone" id="phone"/> <img style={{width:"5%"}} src={check} alt="" srcset=""/><br/>
                        <label>Password             :  </label><input type="password" onChange={handlePassword} name="password" id="password"/>              <br/>
                        <label>Confirm Password     :  </label><input type="password" onChange={handleConfirmPassword} name="confirmpassword" id="confirmpassword"/> <img style={{width:"5%"}} src={checkconfirmpassword} alt="" srcset=""/><br/>

                        <br/>

                        <label>Jurusan Sekolah      :  </label><input type="text" name="jurusan_sekolah" id="jurusan_sekolah"/><br/>    
                        <label>Nama Sekolah      :  </label><input type="text" name="nama_sekolah" id="nama_sekolah"/><br/>
                        
                        <label>Education            :  </label><select name="education" id="education" onChange={(e) => setSelectEducation(e.target.value)}>
                                                                    <option value="SMA">SMA</option>
                                                                    <option value="SMP">SMP</option>
                                                                    <option value="SD">SD</option>
                                                                    <option value="S1">S1</option>
                                                                    <option value="S2">S2</option>
                                                                    <option value="S3">S3</option>
                                                                </select>                                                          <br/>
                        <label>Status               :  </label><select name="status" id="status" onChange={(e) => setSelectStatus(e.target.value)}>
                                                                    <option value="Single">Single</option>
                                                                    <option value="In a Relationship">In a Relationship</option>
                                                                    <option value="Married">Married</option>
                                                                    <option value="Widow / Widowed">Widow / Widowed</option>
                                                                </select>                                                          <br/>
                        <label>Gender               :  </label>
                                                        <input type="radio" id="Male" name="gender" value="Male" checked={ selectGender==="Male" } onChange={ () => setSelectGender("Male") }/>
                                                        &nbsp;<img src="https://img.icons8.com/cotton/64/000000/male.png"/>
                                                        &emsp;
                                                        <input type="radio" id="Female" name="gender" value="Female" checked={ selectGender==="Female" } onChange={ () => setSelectGender("Female") }/>
                                                        &nbsp;<img src="https://img.icons8.com/cotton/64/000000/female.png"/>    
                        <br/><br/><br/>                                                                                                     <br/><br/>
                        <button onClick={() => history.push("/")}>Go To Login</button> 
                    </div>
                
                    {/* ================================= FORM GAJI ================================= */}
                    <div className="col-md-6" style={{paddingLeft:"30px"}}>
                        <h4><b>Data Salary</b></h4>

                        <label>NIK                  :  </label><input type="text" name="nik" id="nik"/><br/>
                        <label>No Induk Karyawan :  </label><input type="text" name="nokaryawan" id="nokaryawan"/><br/>
                        <label>NPWP :  </label><input type="text" name="npwp" id="npwp"/><br/>
                        <label>Rekening BCA :  </label><input type="text" name="nobca" id="nobca"/><br/>
                        <label>BPJS Kesehatan :  </label><input type="text" name="nokesehatan" id="nokesehatan"/><br/>
                        <label>BPJS Ketenagakerjaan :  </label><input type="text" name="noketenagakerjaan" id="noketenagakerjaan"/><br/>             
                        <label>Tanggal Masuk Kerja           :  </label><input type="date" name="tglkerja" id="tglkerja"/> <br/>
                        <label>Status Karyawan           :  </label><select name="status_karyawan" id="status_karyawan">
                                                                <option value="PKWT">PKWT</option>
                                                                <option value="PKWTT">PKWTT</option>
                                                                <option value="OS">OS</option>
                                                            </select><br/>

                        <label>Department           :   </label><select name="department" id="department" onChange={(e) => setSelectDepartment(e.target.value)}>
                                                                    <option value="Compound">Compound</option>
                                                                    <option value="Mandrel">Mandrel</option>
                                                                    <option value="Extrude">Extrude</option>
                                                                    <option value="TPR_PROSES">TPR PROSES</option>
                                                                    <option value="TPR_COMPOUND">TPR COMPOUND</option>
                                                                    <option value="Teknik">Teknik</option>
                                                                    <option value="Gudang">Gudang</option>
                                                                    <option value="QC">QC</option>
                                                                    <option value="HRGA">HRGA</option>
                                                                    <option value="TUKANG">TUKANG</option>
                                                                    <option value="PPIC">PPIC</option>
                                                                    <option value="R&D">R&D</option>
                                                                    <option value="S&M">S&M</option>
                                                                    <option value="EXIM">EXIM</option>
                                                                    <option value="F&A">F&A</option>
                                                                    <option value="Purchasing">Purchasing</option>
                                                                    <option value="IT">IT</option>
                                                                    <option value="Manajemen">Manajemen</option>
                                                                    <option value="admin">admin</option>
                                                        </select><br/>

                        <label>Position             :  </label><select name="position" id="position" onChange={(e) => setSelectPosition(e.target.value)}>
                                                            <option value="Manager">Manager</option>
                                                            <option value="Staff">Staff</option>
                                                            <option value="admin">admin</option>
                                                        </select><br/> <br/>

                        <label>Basic Salary         :  </label><input type="text" name="basic_salary" id="basic_salary"/>          <br/>
                                                        <br/> 
                        <br/> 
                        <label>Photo                :  </label><input type="file" name="image" onChange={ e => setImage(e.target.files[0]) }/><br/>
                        
                    </div>
                    <input type="submit" value="Register" style={{position:"absolute",right:"0", marginRight:"5%", bottom:"0", marginBottom:"11%"}}/> 
                </div>
            </form>
            <center>
                <form onSubmit={processRegisterExcel} style={{position:"absolute",left:0,marginLeft:"33%",marginTop:"5%"}}>
                    <label>Insert Employee(excel)                :  </label><input type="file" name="excel" onChange={ e => setExcel(e.target.files[0]) }/>
                    
                    <input type="submit" value="Submit"/> 
                    <br/><br/>
                    <ReactHTMLTableToExcel
                            id="test-table-xls-button"
                            className="download-table-xls-button"
                            table="template_register"
                            filename="template_register"
                            sheet="Sheet1"
                            buttonText="Download Template"/>
                            <table id="template_register" hidden>
                                <tr>
                                    <th>Name</th>
                                    <th>Address</th>
                                    <th>BirthDate</th>
                                    <th>City</th>
                                    <th>Phone</th>
                                    <th>Password</th>

                                    <th>Jurusan Sekolah</th>
                                    <th>Nama Sekolah</th>
                                    <th>Education</th>
                                    <th>Status</th>
                                    <th>Gender</th>

                                    <th>NIK</th>
                                    <th>No Induk Karyawan</th>
                                    <th>NPWP</th>
                                    <th>Rekening BCA</th>
                                    <th>BPJS Kesehatan</th>
                                    <th>BPJS Ketenagakerjaan</th>
                                    <th>Tanggal Masuk Kerja</th>
                                    <th>Status Karyawan</th>
                                    <th>Department</th>
                                    <th>Position</th>

                                    <th>Basic Salary</th>
                                </tr>
                                <tr>
                                    <td>hanya boleh huruf, maksimal 255 karakter</td>
                                    <td>maksimal 255 karakter</td>
                                    <td>YYYY-MM-DD, contoh : 2020-07-02</td>
                                    <td>maksimal 255 karakter</td>
                                    <td>hanya boleh angka, maksimal 20 angka</td>
                                    <td>maksimal 255 karakter</td>
                                    <td>Pilihan : IPA, IPS atau BAHASA</td>
                                    <td>maksimal 255 karakter (nama sekolah terakhir)</td>
                                    <td>Pilihan : SMA, SMP, SD, S1, S2 atau S3</td>
                                    <td>Pilihan : Single, In a Relationship, Married, Widow / Widowed</td>
                                    <td>Pilihan : Male, Female</td>
                                    <td>hanya boleh angka</td>
                                    <td>maksimal 255 karakter</td>
                                    <td>hanya boleh angka</td>
                                    <td>hanya boleh angka</td>
                                    <td>hanya boleh angka</td>
                                    <td>maksimal 255 karakter</td>
                                    <td>YYYY-MM-DD, contoh : 2020-07-02</td>
                                    <td>Pilihan : PKWT, PKWTT, OS</td>
                                    <td>Pilihan : Compound, Mandrel, Extrude, TPR PROSES, TPR COMPOUND, Teknik, Gudang, QC, HRGA, TUKANG, PPIC, R&D, S&M, EXIM, F&A, Purchasing, IT, Management, admin</td>
                                    <td>Pilihan : Manager, Staff, admin</td>
                                    <td>hanya boleh angka</td>
                                </tr>
                            </table>
                            <br/><br/>
                </form> 
            </center>
                        
            
        </>
     );
}
 
export default Register;