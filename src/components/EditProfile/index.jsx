import React, { useState, useEffect } from 'react';
import { useHistory } from 'react-router-dom';
import NavBar from "../NavBar/";
import './style.css';

const EditProfile = () => {
    const history= useHistory();
    const token = window.sessionStorage.getItem("token");
    const [ dataUser, setDataUser ] = useState([]);
    const [ foto, setFoto ] = useState([]);

    //FIELD EDIT PROFILE
    const [image, setImage] = useState("");
    const [firstname, setFirstname] = useState("");
    const [lastname, setLastname] = useState("");
    const [address, setAddress] = useState("");
    const [birthDate, setBirthDate] = useState("");
    const [city, setCity] = useState("");
    const [status, setStatus] = useState("");
    const [gender, setGender] = useState("");
    

    async function getDataUser(){
        try {
            const res = await fetch(`/api/getDataUser?token=${token}`, { 
                method : "GET",
                headers : { "Content-Type" : "application/json" }
            });
            let a = await res.json();
            setDataUser(a[0].hasil);
            setFoto(a[0].gambar);
            fillStateEditProfile(a[0].hasil);
        } catch (error) {
            console.error(error);
        }
    }
    useEffect(() => {
        getDataUser();
    }, []);

    function fillStateEditProfile(item){
        setFirstname(item.name);
        setAddress(item.address);
        setBirthDate(item.birth_date_update);
        setCity(item.city);
        setStatus(item.status);
        setGender(item.gender);
    }

    async function processEditProfile(e){
        e.preventDefault();
        const formData= new FormData();
        formData.append('name', firstname + " " + lastname);
        formData.append('address',address);
        formData.append('birthDate',birthDate);
        formData.append('city',city);
        formData.append('status',status);
        formData.append('gender',gender);
        formData.append('token',token);
        formData.append('image',image);
        try {
            const res = await fetch("/listkaryawan/updateProfile", { 
                method : "POST",
                body : formData
            });
            let a = await res.json();
            alert(a[0].pesan);
            if(a[0].pesan=="Success Edit Profil")window.location.reload();
        } catch (error) {
            console.error(error);
        }
    }

    return ( 
        <>
            <NavBar/>
            <center>
                <h1>Edit Profile</h1>
                <img src={"data:image/png;base64, "+foto.split1+foto.split2} style={{width:"200px", height:"200px"}}/><br/>
                <input type="file" name="image" id="image" onChange={ e => setImage(e.target.files[0]) }/>
            </center><br/>
            <form onSubmit={processEditProfile}>
                <div className='row' style={{width:"100%",fontFamily:"arial"}}>
                    <div className="col-md-4"></div>
                    <div className="col-md-8">
                            <label>Name               :  </label><input type="text" name="firstname"     value={firstname}           onChange={(e) => setFirstname(e.target.value)}          />  <br/>

                            <label>Address                  :  </label><input type="text" name="address"       value={address}             onChange={(e) => setAddress(e.target.value)}            />  <br/>
                            <label>Birth Date               :  </label><input type="date" name="birth_date"    defaultValue={birthDate}    onChange={(e) => setBirthDate(e.target.value)}          />  <br/>
                            <label>City                     :  </label><input type="text" name="city"          value={city}                onChange={(e) => setCity(e.target.value)}               />  <br/>
                            <label>Phone                    :  </label><input type="text" name="phone"         value={dataUser.phone}                                                      disabled/>  <br/>
                            <label>Education                :  </label><input type="text" name="education"     value={dataUser.education}                                                  disabled/>  <br/>
                            <label>Department               :  </label><input type="text" name="department"    value={dataUser.department}                                                 disabled/>  <br/>
                            <label>Position                 :  </label><input type="text" name="position"      value={dataUser.position}                                                   disabled/>  <br/>
                            <label>Status                   :  </label><select            name="status"                                 onChange={(e) => setStatus(e.target.value)}>
                                    {status == "Single" ?               <option value="Single"              selected>Single             </option> : <option value="Single">             Single              </option>}
                                    {status == "In a Relationship" ?    <option value="In a Relationship"   selected>In a Relationship  </option> : <option value="In a Relationship">  In a Relationship   </option>}
                                    {status == "Married" ?              <option value="Married"             selected>Married            </option> : <option value="Married">            Married             </option>}
                                    {status == "Widow / Widowed" ?      <option value="Widow / Widowed"     selected>Widow / Widowed    </option> : <option value="Widow / Widowed">    Widow / Widowed     </option>}
                                </select> <br/>
                            <label>Gender                   :  </label><select            name="gender"                                    onChange={(e) => setGender(e.target.value)}>
                                    {gender == "Male" ?      <option value="Male"    selected>Male   </option> : <option value="Male">   Male    </option>}
                                    {gender == "Female" ?    <option value="Female"  selected>Female </option> : <option value="Female"> Female  </option>}
                                </select>    <br/>
                            <label>Jurusan Sekolah          :  </label><input type="text" name=""         value={dataUser.jurusan_sekolah}                                                          disabled/>                                                                                                                  <br/>  
                            <label>Nama Sekolah             :  </label><input type="text" name=""         value={dataUser.nama_sekolah}                                                             disabled/>                                                                                                                  <br/>
                            <label>NIK                      :  </label><input type="text" name=""         value={dataUser.nik}                                                                      disabled/>                                                                                                                  <br/>
                            <label>No Karyawan              :  </label><input type="text" name=""         value={dataUser.nokaryawan}                                                               disabled/>                                                                                                                  <br/>
                            <label>NPWP                     :  </label><input type="text" name=""         value={dataUser.npwp}                                                                     disabled/>                                                                                                                   <br/>
                            <label>No Rekening BCA          :  </label><input type="text" name=""         value={dataUser.nobca}                                                                    disabled/>                                                                                                                 <br/>
                            <label>No BPJS Kesehatan        :  </label><input type="text" name=""         value={dataUser.nokesehatan}                                                              disabled/>                                                                                                                 <br/>
                            <label>No BPJS Ketenagakerjaan  :  </label><input type="text" name=""         value={dataUser.noketenagakerjaan}                                                        disabled/>                                                                                                                <br/>
                            <label>Status Karyawan          :  </label><input type="text" name=""         value={dataUser.status_karyawan}                                                          disabled/>                                                                                                                   <br/>
                            
                    </div>
                </div>
                <br/>
                <center>
                    <input type="submit" value="update"/>
                </center>
            </form>
            {/* <button onClick={() => history.push("/home")}>Back</button> */}
        </>
     );
}
 
export default EditProfile;